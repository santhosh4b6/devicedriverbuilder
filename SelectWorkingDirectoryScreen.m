classdef SelectWorkingDirectoryScreen < BreadCrumb

    %SelectWorkingDirectoryScreen is one screen that is meant
    %to be included in a package of screens that make up a Device Driver Builder app. It presents user a way to specify working directory and the directory where 3p files are reciding.

    % Copyright 2023 The MathWorks, Inc.

    properties(Access = public)
        ScreenDescription
        CurrentWorkingDirDescription
        SelectDriverFolderDescription
        CurrentDirectoryText
        CurrentDirectory
        BrowseButton
        DeleteButton
        ValidateEditText
        Table
    end
    
    properties(Access = private, Constant = true)
        FontSize = 13
        BreadCrumbStep = 2
    end

    methods(Access = 'public')
        function obj = SelectWorkingDirectoryScreen(workflow)
            validateattributes(workflow, {'matlab.hwmgr.internal.hwsetup.Workflow'}, {});
            obj@BreadCrumb(workflow,matlab.hwmgr.internal.hwsetup.TemplateLayout.GRID);
            obj.setCurrentStep(obj.BreadCrumbStep);
            obj.ContentGrid.RowHeight = {'fit', 22, 22, 22, 'fit'};
            obj.ContentGrid.ColumnWidth = {10, 'fit', '1x', 100,100};
           
            obj.ScreenDescription = matlab.hwmgr.internal.hwsetup.Label.getInstance(obj.ContentGrid);
            obj.ScreenDescription.Text = "Select the working directory and folders of the source files";
            obj.ScreenDescription.FontWeight = 'bold';
            
            obj.CurrentWorkingDirDescription = matlab.hwmgr.internal.hwsetup.HTMLText.getInstance(obj.ContentGrid);
            obj.CurrentWorkingDirDescription.Text = "Select the working directory";

            obj.SelectDriverFolderDescription = matlab.hwmgr.internal.hwsetup.HTMLText.getInstance(obj.ContentGrid);
            obj.SelectDriverFolderDescription.Text = "Select the folders where source files(third-party) of the driver are present.";
            
            obj.ValidateEditText = matlab.hwmgr.internal.hwsetup.EditText.getInstance(obj.ContentGrid);        
            obj.ValidateEditText.TextAlignment = 'left';
           
            % Set BrowseButton Properties
            obj.BrowseButton = matlab.hwmgr.internal.hwsetup.Button.getInstance(obj.ContentGrid);
            obj.BrowseButton.Text = 'Add folder';
            obj.BrowseButton.Color = matlab.hwmgr.internal.hwsetup.util.Color.HELPBLUE;
            obj.BrowseButton.ButtonPushedFcn = @obj.browseDirectory;

            
            obj.DeleteButton = matlab.hwmgr.internal.hwsetup.Button.getInstance(obj.ContentGrid);
            obj.DeleteButton.Text = 'Remove folder';
            obj.DeleteButton.Color = matlab.hwmgr.internal.hwsetup.util.Color.HELPBLUE;
            obj.DeleteButton.ButtonPushedFcn = @obj.removeRow;

            obj.CurrentDirectoryText = matlab.hwmgr.internal.hwsetup.EditText.getInstance(obj.ContentGrid);
            obj.CurrentDirectoryText.TextAlignment = 'left';
            obj.CurrentDirectoryText.Text = pwd;

            % Set BrowseButton Properties
            obj.CurrentDirectory = matlab.hwmgr.internal.hwsetup.Button.getInstance(obj.ContentGrid);
            obj.CurrentDirectory.Text = 'Select';
            obj.CurrentDirectory.Color = matlab.hwmgr.internal.hwsetup.util.Color.HELPBLUE;
            % Set callback when finish button is pushed
            obj.CurrentDirectory.ButtonPushedFcn = {@obj.chooseWorkingDirectory};

            obj.ScreenDescription.Row = 1;
            obj.ScreenDescription.Column = [1,  4];

            obj.CurrentWorkingDirDescription.Row = 2;
            obj.CurrentWorkingDirDescription.Column = [1,  4];

            obj.CurrentDirectoryText.Row = 3;
            obj.CurrentDirectoryText.Column = [1, 3];

            obj.CurrentDirectory.Row = 3;
            obj.CurrentDirectory.Column = 4;

            obj.SelectDriverFolderDescription.Row = 4;
            obj.SelectDriverFolderDescription.Column = [1,  5];

            obj.ValidateEditText.Row = 5;
            obj.ValidateEditText.Column = [1, 3];

            obj.BrowseButton.Row = 5;
            obj.BrowseButton.Column = 4;

            obj.DeleteButton.Row = 5;
            obj.DeleteButton.Column = 5;
            
            obj.Table = matlab.hwmgr.internal.hwsetup.Table.getInstance(obj.ContentGrid);
            obj.Table.Tag = 'Example_Table';

            obj.Table.Row = 6;
            obj.Table.Column = [1, 5];
            obj.Table.ColumnEditable = logical([0]);
            updateScreen(obj);
            obj.HelpText.AboutSelection = '';
            obj.HelpText.WhatToConsider = 'In order to select the folders of the drivers please download it from the internet. For example if you want to select the Adxl345 files go to and download the files and save it in a folder.';
        end

        function id = getPreviousScreenID(~)
                id = 'IntroductionScreen';
        end

        function  id = getNextScreenID(obj)           
                id = 'SelectSourceFilesScreen';            
        end

        function removeRow(obj,~,~)
            manageTableRows(obj.Table, 'remove');
            obj.Workflow.FolderPath = obj.Table.Data;
        end
        
        function addRow(obj,~)
            obj.Table.Data =  [obj.Table.Data; table({obj.ValidateEditText.Text})];
        end
        function reinit(obj)
            updateScreen(obj);
        end
        
        function show(obj)
            %Overwrite show method to hide Back button on Mac and Linux
            %since it is the entry screen for postinstall setup
            show@matlab.hwmgr.internal.hwsetup.TemplateBase(obj)
            if isunix
                obj.BackButton.Visible = 'off';
            end
        end
    end

    methods(Access = 'private')
        function updateScreen(obj)
        end
        
        function updateSetupFlag(obj, src, ~)
        end

        function browseDirectory(obj,~,~)
            % browseDirectory - Callback when browse button is pushed that launches the
            % file browsing window set to the directory indicated by obj.ValidateEditText.Text
            dir = uigetdir(obj.ValidateEditText.Text);
            if dir % If the user cancels the file browser, uigetdir returns 0.
                % When a new location is selected, then set that location value
                % back to show it in edit text area. (ValidateEditText.Text).
                obj.ValidateEditText.Text = dir;
                addRow(obj);
            end
            obj.Workflow.Window.bringToFront();
        end

        function chooseWorkingDirectory(obj,~,~)
            % browseDirectory - Callback when browse button is pushed that launches the
            % file browsing window set to the directory indicated by obj.ValidateEditText.Text
            dir = uigetdir(obj.CurrentDirectoryText.Text);
            if dir % If the user cancels the file browser, uigetdir returns 0.
                % When a new location is selected, then set that location value
                % back to show it in edit text area. (ValidateEditText.Text).
                obj.CurrentDirectoryText.Text = dir;
            end
            obj.Workflow.Window.bringToFront();
        end        
    end

end
