classdef FileGenerationScreen < BreadCrumb
    %FileGenerationScreen is one screen that is meant
    %to be included in a package of screens that make up a Device Driver Builder app. It generates the system object files.

    % Copyright 2023 The MathWorks, Inc.

    properties(Access = public)
        ResultLabel
        FolderText
        ListText
        FolderTextLabel
    end
    
    properties(Access = private, Constant = true)
        FontSize = 13
        BreadCrumbStep = 8
    end

    methods(Access = 'public')
        function obj = FileGenerationScreen(workflow)
            validateattributes(workflow, {'matlab.hwmgr.internal.hwsetup.Workflow'}, {});
            obj@BreadCrumb(workflow,matlab.hwmgr.internal.hwsetup.TemplateLayout.GRID);
            obj.setCurrentStep(obj.BreadCrumbStep);
            obj.ContentGrid.RowHeight = {22,22,22,22,22,22};
            obj.ContentGrid.ColumnWidth = {'fit','fit','fit'};
            obj.ResultLabel = matlab.hwmgr.internal.hwsetup.Label.getInstance(obj.ContentGrid);
            obj.ResultLabel.Text = 'Please click on the Generate button to generate the system object related files.';
            obj.ResultLabel.Row = 1;
            obj.ResultLabel.Column = [1, 3];
            obj.ResultLabel.FontWeight = 'bold';
            obj.FolderText = matlab.hwmgr.internal.hwsetup.Label.getInstance(obj.ContentGrid);
            obj.FolderText.Text = 'Files will be generated in the below location';
            obj.FolderText.Row = 2;
            obj.FolderText.Column = [1, 3];
            obj.FolderTextLabel = matlab.hwmgr.internal.hwsetup.Label.getInstance(obj.ContentGrid);
            obj.FolderTextLabel.Row = 3;
            obj.FolderTextLabel.Column = [1, 3];
            obj.FolderTextLabel.FontWeight = 'bold';
            obj.ListText = matlab.hwmgr.internal.hwsetup.Label.getInstance(obj.ContentGrid);
            obj.ListText.Text = 'List of all files which are generated 1.m,2.cpp,3.h';
            obj.ListText.Row = 4;
            obj.ListText.Column = [1, 3];
            obj.NextButton.Text = 'Generate';           
            updateScreen(obj);
            obj.HelpText.AboutSelection = '';
            obj.HelpText.WhatToConsider = 'There are three types of generated files  1. .m 2. .cpp 3. .h . .cpp file has two methods 1)init 2)loop. Please fill these two methods with the respective function calls from the 3p. For example if the task is to capture acceleration data from adxl345 sensor please open readsample.ino example from the github and copy paste the code of setup and loop of .ino to our generated .cpp file. Then assign the acquired samples to the input parameters of the loop function call.';
        end

        function id = getPreviousScreenID(~)
            id = 'PreviewScreen';
        end

        function  id = getNextScreenID(obj)
            id = 'FileGenerationScreen';
        end

        function reinit(obj)
            updateScreen(obj);
        end
        
        function show(obj)
            %Overwrite show method to hide Back button on Mac and Linux
            %since it is the entry screen for postinstall setup
            show@matlab.hwmgr.internal.hwsetup.TemplateBase(obj)
            if isunix
                obj.BackButton.Visible = 'off';
            end
        end
    end

    methods(Access = 'private')
        function updateScreen(obj)

        end
        
        function updateSetupFlag(obj, src, ~)
           
        end
    end

end

