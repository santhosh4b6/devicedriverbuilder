classdef PreviewScreen < BreadCrumb
    %PreviewScreen is one screen that is meant
    %to be included in a package of screens that make up a Device Driver Builder app. It presents with the preview of system object.

    % Copyright 2023 The MathWorks, Inc.
    properties(Access = public)
        PreviewLabel
        BlockImage
    end

    properties(Access = private, Constant = true)
        FontSize = 13
        BreadCrumbStep = 7
    end

    methods(Access = 'public')
        function obj = PreviewScreen(workflow)
            validateattributes(workflow, {'matlab.hwmgr.internal.hwsetup.Workflow'}, {});
            obj@BreadCrumb(workflow,matlab.hwmgr.internal.hwsetup.TemplateLayout.GRID);
            obj.setCurrentStep(obj.BreadCrumbStep);
            % obj.NextButton.Text = 'Generate';
            obj.ContentGrid.RowHeight = {'1x','1x','1x'};
            obj.ContentGrid.ColumnWidth = {'1x','1x','1x'};
            obj.PreviewLabel = matlab.hwmgr.internal.hwsetup.Label.getInstance(obj.ContentGrid);
            obj.PreviewLabel.Text = 'Preview of the block with inputs and outputs is shown below:';
            obj.PreviewLabel.Row = 1;
            obj.PreviewLabel.Column = [1, 3];
            obj.PreviewLabel.FontWeight = 'bold';
            obj.BlockImage = matlab.hwmgr.internal.hwsetup.Image.getInstance(obj.ContentGrid);
            obj.BlockImage.Row = 2;
            obj.BlockImage.Column = 2;
            updateScreen(obj);
            obj.HelpText.AboutSelection = '';
            obj.HelpText.WhatToConsider = 'Preview image shows the inputs and outputs of the block which are defined in the previous screens. If any thing needs to be updated please navigate to the respective previous screen and make the appropriate change.';
        end

        function id = getPreviousScreenID(~)
            id = 'InputScreen';
        end

        function  id = getNextScreenID(obj)
            id = 'FileGenerationScreen';
        end

        function reinit(obj)
            updateScreen(obj);
        end

        function show(obj)
            %Overwrite show method to hide Back button on Mac and Linux
            %since it is the entry screen for postinstall setup
            show@matlab.hwmgr.internal.hwsetup.TemplateBase(obj)
            if isunix
                obj.BackButton.Visible = 'off';
            end
        end
    end

    methods(Access = 'private')
        function updateScreen(obj)

        end

        function updateSetupFlag(obj, src, ~)

        end
    end

end

