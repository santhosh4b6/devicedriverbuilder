classdef(Abstract) TemplateBase < handle
    % TEMPLATEBASE defines the common widgets and methods required for
    % rendering and interacting with the HW Setup screens
    %
    %   TEMPLATEBASE Properties
    %   ContentPanel    Contains template/screen-specific widgets
    %   ContentGrid     Contains template/screen-specific widgets arranged
    %                   using a grid inside ContentPanel
    %   HelpText        Contains the Help Text to aid the user
    %   BackButton      Back Button widget to navigate to previous screen
    %   NextButton      Next Button widget to navigate to Next screen
    %   CancelButton    Cancel Button widget t exit out of the workflow
    %   Title           Title for the screen specified as a Label widget
    %   Workflow        Workflow object that contains data that need to be
    %                   maintained across the workflow
    %
    %   TEMPLATEBASE Methods
    %   show                Display the template/screen
    %   logMessage          log diagnostic messages to a file
    %   getNextScreenID     Return the Next Screen ID (name of the class)
    %   getPreviousScreenID Return the Previous Screen ID (name of the class)

    % Copyright 2016-2022 The MathWorks, Inc.

    properties(Access = {?matlab.hwmgr.internal.hwsetup.TemplateBase, ...
            ?matlab.hwmgr.internal.hwsetup.util.TemplateLayoutManager,...
            ?hwsetuptest.util.TemplateBaseTester,...
            ?hwsetup.testtool.TesterBase})
        % HelpText - The panel that contains information for the customers
        % to successfully interact with the screen
        HelpText
        % Banner - UI Banner
        Banner
        % NavigationGrid - The grid which contains buttons to navigate
        % back and forth and cancel out of the HW Setup workflow
        NavigationGrid
        %ParentGrid- The outermost grid that includes the Content,
        %Navigation, Help and Banner sections.
        ParentGrid
        % ContentPanel - HW Setup Panel widget that contains the template
        % specific widgets. These widgets are defined by the individual
        % Template classes. The properties for these widgets should be
        % specified in the initializeScreen() method
        ContentPanel
        % ContentGrid - HW Setup Panel widget that contains the template
        % specific widgets. These widgets are defined by the individual
        % Template classes. The properties for these widgets should be
        % specified in the initializeScreen() method
        ContentGrid
        % BackButton - HW Setup Button widget that on clicking navigates the user
        % to the previous screen.
        BackButton
        % NextButton - HW Setup Button widget that on clicking navigates the user
        % to the next screen.
        NextButton
        % CancelButton - HW Setup Button widget that on clicking exits out of the
        % HW Setup workflow
        CancelButton
        % Workflow - Class of type matlab.hwmr.internal.hwsetup.Workflow
        % that stores data required to be maintained across the life-time
        % of the HW Setup workflow
        Workflow
        % Title - HW Setup Label widget that defines the title of the
        % screen
        Title
    end

    properties (Access = {?matlab.hwmgr.internal.hwsetup.TemplateBase,...
            ?hwsetuptest.util.TemplateBaseTester})
        % TemplateLayout enum that specifies which layout type is used to
        % arrange widgets inside ContentPanel
        TemplateLayout
    end

    properties (Access = ?hwsetuptest.util.TemplateBaseTester)
        % WorkflowSteps - Area in the banner that displays the steps in the
        % workflow. To control the display, refer to Steps property of
        % Workflow class.
        WorkflowSteps
    end

    methods
        function obj = TemplateBase(varargin)
            % TEMPLATEBASE constructor
            narginchk(1, 2);
            validateattributes(varargin{1}, {'matlab.hwmgr.internal.hwsetup.Workflow'}, {'nonempty'});

            % Set the Workflow property
            obj.Workflow = varargin{1};
            if nargin == 1
                obj.TemplateLayout = matlab.hwmgr.internal.hwsetup.TemplateLayout.MANUAL;
            else
                obj.TemplateLayout = varargin{2};
            end

            % Get all the widgets that need to be created as a part of the
            % Base Template
            widgetList = matlab.hwmgr.internal.hwsetup.TemplateBase.getAllTemplateWidgetLayoutDetails();
            % Get the specifications i.e Position, Color etc. for each of the widgets
            widgetPropertyMap = matlab.hwmgr.internal.hwsetup.util.TemplateLayoutManager.getAllTemplateWidgetLayoutDetails();
            for i = 1:numel(widgetList)
                % Create the widgets and parent them
                if strcmpi(widgetList(i).Parent, 'DEFAULT')
                    parent = obj.Workflow.Window;
                else
                    parent = obj.(widgetList(i).Parent);
                end
                obj.(widgetList(i).Name) = matlab.hwmgr.internal.hwsetup.util.TemplateLayoutManager.addWidget(...
                    widgetList(i).Type, parent);

                % Assert if the widget property defaults are not predefined
                if ~widgetPropertyMap.isKey(widgetList(i).Name)
                    assert(['Widget properties for ' widgetList(i).Name ...
                        ' not defined in matlab.hwmgr.internal.hwsetup.util.TemplateLayoutManager.getAllTemplateWidgetLayoutDetails']);
                end

                % Set widget properties
                matlab.hwmgr.internal.hwsetup.util.TemplateLayoutManager.setWidgetProperties(...
                    widgetPropertyMap(widgetList(i).Name), obj.(widgetList(i).Name));
            end

            % Add callbacks to the Back, Next and Cancel Buttons
            obj.BackButton.ButtonPushedFcn = {@matlab.hwmgr.internal.hwsetup.TemplateBase.backButtonCallback, obj};
            obj.NextButton.ButtonPushedFcn = {@matlab.hwmgr.internal.hwsetup.TemplateBase.nextButtonCallback, obj};
            % If the screen is the Last one in the workflow, set the "Next"
            % Button text to "Finish" and update the callback for the
            % Button
            if obj.isScreenLast
                obj.NextButton.Text = message('hwsetup:template:FinishButtonText').getString;
                obj.NextButton.ButtonPushedFcn = {@matlab.hwmgr.internal.hwsetup.TemplateBase.finish, obj};
            end
            obj.CancelButton.ButtonPushedFcn = {@matlab.hwmgr.internal.hwsetup.TemplateBase.finish, obj};
            % Set the Current screen property on the Workflow object
            obj.Workflow.CurrentScreen = obj;
            % hide ContentPanel to prevent users from seeing initial
            % properties before the updated values are rendered
            obj.ContentPanel.Visible = 'off';

            if obj.TemplateLayout == matlab.hwmgr.internal.hwsetup.TemplateLayout.GRID
                % enable grid layout if set for screen or template
                obj.ContentGrid.Visible = 'on';
            end

            % if no steps are registered, hide the widget and center align
            % the title
            if isempty(obj.Workflow.Steps)
                obj.WorkflowSteps.Visible = 'off';
                obj.Banner.RowHeight = {'1x', 'fit', '1x'};
            else
                obj.setActiveStep(obj.Workflow.ActiveStep);
            end
        end

        function show(obj)
            %SHOW - Display the HW Setup Screen
            %   show(obj) displays the HW Setup window and the widgets
            %   contained within it.

            obj.logMessage(['Displaying -- ' obj.Workflow.Name ':' class(obj)])
            % If screen is the First one in workflow, set the "Back" button
            % visibility to off
            if obj.isScreenFirst
                % Set the Tag for the Window
                obj.Workflow.Window.Tag = matlab.hwmgr.internal.hwsetup.getNameTag(...
                    [obj.Workflow.Name '_HWSetupWindow']);
                % Set visibility of Back button to off
                obj.BackButton.Visible = 'off';
            end

            % TODO: Remove these updates to Banner once downstream teams
            % transition to use infrastructure to register steps.
            if numel(obj.Banner.Children) == 3
                obj.Banner.Children{3}.Row = 1;
                obj.Banner.RowHeight = {'fit', '1x'};
            end

            % Display the Window
            obj.Workflow.Window.show();
            % Set widget tags
            matlab.hwmgr.internal.hwsetup.TemplateBase.setWidgetTags(obj);
            % Display the widgets
            obj.showAllWidgets();

            % the current visible screen, if different, needs to be hidden
            % before displaying the screen that called show
            if ~isequal(class(obj), class(obj.Workflow.CurrentScreen))
                obj.Workflow.CurrentScreen.ParentGrid.Visible = 'off';
            end

            obj.ParentGrid.Visible = 'on';
        end

        function delete(obj)
            % DELETE - Deletes the widget properties
            obj.deleteAllWidgets();
        end
    end

    methods(Access = protected)
        function disableScreen(obj, widgetsToEnable)
            % DISABLESCREEN - Disables all widgets on the screen

            if ~exist('widgetsToEnable', 'var')
                widgetsToEnable = {};
            end

            if ~iscellstr(widgetsToEnable)
                error(message('hwsetup:widget:InvalidDataType', 'Input to customDisableScreen method',...
                    'cell array of character vectors'))
            end

            widgetNames = matlab.hwmgr.internal.hwsetup.TemplateBase.getWidgetPropertyNames(obj);

            for i = 1:numel(widgetNames)
                widget = obj.(widgetNames{i});

                if(isa(widget, 'matlab.hwmgr.internal.hwsetup.mixin.EnableWidget') && ...
                        isprop(widget, 'Enable')) && ~ismember(widgetNames{i}, widgetsToEnable)
                    widget.Enable = 'off';

                end
            end

            drawnow;
        end

        function enableScreen(obj)
            % ENABLESCREEN - Enables all widgets on the screen

            obj.ParentGrid.enable();
            widgetNames = matlab.hwmgr.internal.hwsetup.TemplateBase.getWidgetPropertyNames(obj);
            for i = 1:numel(widgetNames)
                widget = obj.(widgetNames{i});
                if(isa(widget, 'matlab.hwmgr.internal.hwsetup.mixin.EnableWidget') && ...
                        isprop(widget, 'Enable'))
                    widget.Enable = 'on';
                end
            end
            drawnow;
        end

        function logMessage(screen, str)
            % LOGMESSAGE - logs message specified by STR in the logfile

            validateattributes(str, {'char', 'string'},...
                {'nonempty'});

            if ~isempty(screen.Workflow.HWSetupLogger)
                screen.Workflow.HWSetupLogger.log(str);
            end
        end

        function setActiveStep(obj, value)
            % SETACTIVESTEP - highlights the given step in the workflow
            % at index specified by value

            validateattributes(value, {'double'}, {'>', 0, '<=' numel(obj.Workflow.Steps)});

            steps = obj.Workflow.Steps;
            obj.Workflow.ActiveStep = value;

            % format text for display
            steps{value} = ['<font color="white"><b>', steps{value}, '</b></font>'];
            obj.WorkflowSteps.Text = sprintf('<font color="#9A9A9A">%s</font>',...
                strjoin(steps, ' > '));
        end
    end

    methods(Access = private)
        function hide(obj)
            obj.ParentGrid.Visible = 'off';
        end

        function showAllWidgets(obj)
            % SHOWALLWIDGETS - iterates through the specified widgets and
            % calls the "show" method on each one to display the widgets

            % Get all the widgets
            allWidgets = matlab.hwmgr.internal.hwsetup.TemplateBase.getWidgetPropertyNames(obj);
            for i = 1:numel(allWidgets)
                if isvalid(obj.(allWidgets{i})) && isequal(obj.(allWidgets{i}).Visible, 'on')
                    obj.(allWidgets{i}).show();
                end
            end
            obj.ContentPanel.Visible = 'on';
        end

        function deleteAllWidgets(obj)
            % DELETEALLWIDGETS - Method that deletes the top-level container -
            % parentPanel that will trigger the deletion of all contained
            % widgets
            if ~isempty(obj.ParentGrid)
                obj.ParentGrid.delete();
            end
        end
    end

    % Methods to be over-ridden in the screen class
    methods

        function reinit(obj) %#ok<MANU>
            % REINIT - Method to set the widget state when a screen is
            %   redisplayed. The screen object will be saved in a map that
            %   lives withing the Workflow class. If a screen is being
            %   redisplayed, the screen object is retrieved from this map.
            %   Additional widget settings can be done using this method
            %

        end

        function screenid = getNextScreenID(obj) %#ok<MANU>
            % GETNEXTSCREENID - Get the screen ID for the next screen in
            % workflow
            %   screenid = getNextScreenID(obj) is a method used to define
            %   the screenid i.e. name of the screen class that comes next
            %   in the workflow. If this is the last screen in the workflow
            %   then the screen class should not override this method
            screenid = [];
        end

        function screenid = getPreviousScreenID(obj) %#ok<MANU>
            % GETPREVIOUSSCREENID - Get the screen ID for the next screen
            % in workflow
            %   screenid = getPreviousScreenID(obj) is a method used to define
            %   the screenid i.e. name of the screen class that comes
            %   before the current screen in the workflow. If this is the
            %   first screen in the workflow then the screen class should
            %   not override this method
            screenid = [];
        end
    end

    % Methods used to determine the Screen sequence, navigation etc.
    methods(Access = {?hwsetuptest.util.TemplateBaseTester, ?matlab.hwmgr.applets.internal.HardwareSetupApplet})

        function out = isScreenFirst(obj)
            % ISSCREENFIRST - Method that determines if a screen is
            % First in the workflow based on if getPreviousScreenID is
            % overridden in the screen class

            out = false;
            mc = metaclass(obj);
            % Get the names for all the methods of the class
            allMethods = {mc.MethodList.Name};
            % Array of if the method was implemented in the base class (TemplateBase)
            % or in the derived class
            definingClasses = {mc.MethodList.DefiningClass};
            idx = ismember(allMethods, 'getPreviousScreenID');
            % If the getPreviousScreenID method is not defined in any of
            % TemplateBase subclasses, this is the first screen.
            if isequal(definingClasses{idx}.Name, 'matlab.hwmgr.internal.hwsetup.TemplateBase')
                out = true;
            end
        end

        function out = isScreenLast(obj)
            % ISSCREENLast - Method that determines if a screen is
            % Last in the workflow based on if getNextScreenID is
            % overridden in the screen class

            out = false;
            mc = metaclass(obj);
            % Get the names for all the methods of the class
            allMethods = {mc.MethodList.Name};
            % Array of if the method was implemented in the base class (TemplateBase)
            % or in the derived class
            definingClasses = {mc.MethodList.DefiningClass};
            idx = ismember(allMethods, 'getNextScreenID');
            % If the getNextScreenID method is not defined in any of
            % TemplateBase subclasses, this is the last screen.
            if isequal(definingClasses{idx}.Name, 'matlab.hwmgr.internal.hwsetup.TemplateBase')
                out = true;
            end
        end

        function out = getNextScreenObj(obj)
            % getNextScreenObj - Method for tests to get the object for the
            % next screen. This method invokes the getNextScreenID method
            % to find the next screen id and creates the screen object for
            % it. The method also saves the current screen object onto the
            % ScreenMap property. If there is not next screen then this
            % method returns []

            out = obj.getScreenObj('next');
        end

        function out = getPreviousScreenObj(obj)
            % getPreviousScreenObj - Method for tests to get the object for the
            % previous screen. This method invokes the getPreviousScreenID method
            % to find the next screen id and creates the screen object for
            % it. The method also saves the current screen object onto the
            % ScreenMap property. If there is not next screen then this
            % method returns []

            out = obj.getScreenObj('previous');
        end
    end

    methods(Access = private)
        function out = getScreenObj(obj, opts)
            validateattributes(obj, {'matlab.hwmgr.internal.hwsetup.TemplateBase'},...
                {'nonempty'});
            opts = validatestring(opts, {'next', 'previous'});

            out = [];

            % Store the context for the existing screen
            obj.Workflow.ScreenMap(class(obj)) = obj;

            switch lower(opts)
                case 'next'
                    screen = obj.getNextScreenID();
                case 'previous'
                    screen = obj.getPreviousScreenID();
            end

            if isempty(screen)
                return
            end

            % Code to save the context of the current screen
            try

                if isKey(obj.Workflow.ScreenMap, screen)
                    % If the screen is available in the map, retrieve the
                    % screen object
                    obj.logMessage(['Loading screen from memory -- ' ...
                        obj.Workflow.Name ':' screen]);
                    out = obj.Workflow.ScreenMap(screen);
                    % call the reinit method to update the settings
                    out.reinit();
                else
                    % Construct the object
                    obj.logMessage(['Initializing screen -- ' ...
                        obj.Workflow.Name ':' screen]);
                    out = feval(screen, obj.Workflow);
                end

                out.Workflow.CurrentScreen = out;
            catch ex
                obj.logMessage(['Error in creating/displaying screen -- ' ...
                    obj.Workflow.Name ':' screen newline 'Details:' ex.message]);
                error(message('hwsetup:template:ScreenLoadError', screen, ex.message));
            end
        end
    end

    methods(Static)

        function nextButtonCallback(src, evt, currentScreen)
            % nextButtonCallback - Callback Handler for the Next Button
            % widget. The activities performed during the "Next" Button
            % Callback
            % 1. Get the ID for the Next Screen
            % 2. Create the screen object for the next screen
            % 3. Save the context of the current screen
            % 4. Delete the current screen
            % 5. Display the next screen

            nextScreenObj = currentScreen.getNextScreenObj();

            if isempty(nextScreenObj)
                return
            end
            if (strcmp(currentScreen.getNextScreenID,'SelectSourceFilesScreen'))
                currentScreen.Workflow.WorkingDir= currentScreen.CurrentDirectoryText.Text;
            else
                if ~strcmp(class(currentScreen),'IntroductionScreen')
                prevScreenObj= currentScreen.getPreviousScreenObj();
                if (strcmp(prevScreenObj.getNextScreenID(),'InputScreen'))
                    prevScreenObj.Workflow.Outputs = prevScreenObj.Table.Data;
                end
                end
            end
            if strcmp(class(currentScreen),'InputScreen')
                currentScreen.Workflow.Inputs = currentScreen.Table.Data;

                workingdirectory=['generateSensorBlockAndTemplateHooks(drvObj,''',pwd,''');']
                % workingdirectory=currentScreen.Workflow.WorkingDir;
                FileRows= nextScreenObj.Workflow.FolderPath.Variables;
                headerfilesPath = '';
                for i=1:length(FileRows)
                    headerfilesPath=[headerfilesPath,['addHeaderPath(drvObj,''',FileRows{i},''');'],newline];
                end
                sourcefilesPath = '';
                for i=1:length(FileRows)
                    sourcefilesPath=[sourcefilesPath,['addSourcePath(drvObj,''',FileRows{i},''');'],newline];
                end
                sourcefiles = '';
                for i=1:length(nextScreenObj.Workflow.SrcFiles)
                    sourcefiles=[sourcefiles,['addSourceFile(drvObj,{''',nextScreenObj.Workflow.SrcFiles{i},'''});'],newline];
                end
                createoutputs = '';
                outputNames = '';
                if height(nextScreenObj.Workflow.Outputs) ~= 0
                    for i=1:length(nextScreenObj.Workflow.Outputs{:,1})
                        createoutputs = [createoutputs,['output_',char(i+64),' = createOutput(drvObj,''',char(nextScreenObj.Workflow.Outputs.(1){i}),''',','''Datatype''',',''',char(nextScreenObj.Workflow.Outputs.(2){i}),''',','''Size''',',',char(nextScreenObj.Workflow.Outputs.(3){i}),');'],newline];
                    end
                end
                addoutput = '';
                if height(nextScreenObj.Workflow.Outputs) ~= 0
                    staticoutput ='addOutput(drvObj,';
                    for i=1:length(nextScreenObj.Workflow.Outputs{:,1})
                        if i==length(nextScreenObj.Workflow.Outputs{:,1})
                            addoutput = [addoutput,['output_',char(i+64)]];
                        else
                            addoutput = [addoutput,['output_',char(i+64),',']];
                        end
                    end
                    staticOutput2 = [staticoutput,addoutput,');',newline];
                else
                    staticOutput2 = '';
                end

                createinputs = '';
                inputNames = '';
                if height(currentScreen.Workflow.Inputs) ~= 0
                    for i=1:length(currentScreen.Workflow.Inputs{:,1})
                        createinputs = [createinputs,['input_',char(i+64),' = createInput(drvObj,''',char(currentScreen.Workflow.Inputs.(1){i}),''',','''Datatype''',',''',char(currentScreen.Workflow.Inputs.(2){i}),''',','''Size''',',',char(currentScreen.Workflow.Inputs.(3){i}),');'],newline];
                    end
                end
                addinput = '';
                if height(currentScreen.Workflow.Inputs) ~= 0
                    staticinput ='addInput(drvObj,';
                    for i=1:length(currentScreen.Workflow.Inputs{:,1})
                        if i==length(currentScreen.Workflow.Inputs{:,1})
                            addinput = [addinput,['input_',char(i+64)]];
                        else
                            addinput = [addinput,['input_',char(i+64),',']];
                        end
                    end
                    staticInput2 = [staticinput,addinput,');',newline];
                else
                    staticInput2 = '';
                end

                createproperty = '';
                propertyNames = '';
                if height(currentScreen.Workflow.MaskParameters) ~= 0
                    for i=1:length(currentScreen.Workflow.MaskParameters{:,1})
                        if strcmp(char(currentScreen.Workflow.MaskParameters.(5){i}),'Non tunable')
                            booleanValue = 'false';
                        else
                            booleanValue = 'true';
                        end
                        createproperty = [createproperty,['property_',char(i+64),' = createProperty(drvObj,''',char(currentScreen.Workflow.MaskParameters.(1){i}),''',','''Datatype''',',''',char(currentScreen.Workflow.MaskParameters.(2){i}),''',','''Size''',',',char(currentScreen.Workflow.MaskParameters.(3){i}),',','''InitValue''',',',currentScreen.Workflow.MaskParameters.(4){i},',','''Tunable''',',',booleanValue,');'],newline];
                    end
                end
                addproperty = '';
                if height(currentScreen.Workflow.MaskParameters) ~= 0
                    staticproperty ='addProperty(drvObj,';
                    for i=1:length(currentScreen.Workflow.MaskParameters{:,1})
                        if i==length(currentScreen.Workflow.MaskParameters{:,1})
                            addproperty = [addproperty,['property_',char(i+64)]];
                        else
                            addproperty = [addproperty,['property_',char(i+64),',']];
                        end
                    end
                    staticProperty2 = [staticproperty,addproperty,');',newline];
                else
                    staticProperty2 = '';
                end


                minputarguments='';
                minputInputarguments='';
                if ((height(nextScreenObj.Workflow.Outputs) ~= 0) | (height(currentScreen.Workflow.Inputs) ~= 0 ))
                    for i=1:length(nextScreenObj.Workflow.Outputs{:,1})
                        if i==length(nextScreenObj.Workflow.Outputs{:,1})
                            minputarguments = [minputarguments,['output_',char(i+64)]];
                        else
                            minputarguments = [minputarguments,['output_',char(i+64),',']];
                        end
                    end
                    if (height(nextScreenObj.Workflow.Outputs) ~= 0 & (height(currentScreen.Workflow.Inputs) ~= 0 ))
                        minputarguments = [minputarguments,','];
                    end
                    for i=1:length(currentScreen.Workflow.Inputs{:,1})
                        if i==length(currentScreen.Workflow.Inputs{:,1})
                            minputInputarguments = [minputInputarguments,['input_',char(i+64)]];
                        else
                            minputInputarguments = [minputInputarguments,['input_',char(i+64),',']];
                        end
                    end
                    minputarguments=['mInputArgumentsToCStepFcn = {',minputarguments,minputInputarguments,'}'];
                end

                minputsetuparguments='';
                if (height(nextScreenObj.Workflow.MaskParameters) ~= 0)
                    for i=1:length(nextScreenObj.Workflow.MaskParameters{:,1})
                        if i==length(nextScreenObj.Workflow.MaskParameters{:,1})
                            minputsetuparguments = [minputsetuparguments,['property_',char(i+64)]];
                        else
                            minputsetuparguments = [minputsetuparguments,['property_',char(i+64),',']];
                        end
                    end
                    % if (size(nextScreenObj.Workflow.MaskParameters) ~= 0)
                    %     minputsetuparguments = [minputsetuparguments,','];
                    % end

                    minputsetuparguments=['mInputArgumentsToCSetupFcn = {',minputsetuparguments,'}'];
                end

                cinputarguments = '';
                cinputInputarguments = '';
                addInputStepArgs = '';
                if ((height(nextScreenObj.Workflow.Outputs) ~= 0) | (height(currentScreen.Workflow.Inputs) ~= 0 ))
                    for i=1:length(nextScreenObj.Workflow.Outputs{:,1})
                        if i==length(nextScreenObj.Workflow.Outputs{:,1})
                            if str2num(char(extractBefore(extractBetween(nextScreenObj.Workflow.Outputs.(3){i},'[',']'),',')))>1
                                if strcmp(char(nextScreenObj.Workflow.Outputs.(2){i}),'single')
                                    cinputarguments = [cinputarguments,['''','float',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint8')
                                    cinputarguments = [cinputarguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int8')
                                    cinputarguments = [cinputarguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint16')
                                    cinputarguments = [cinputarguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int16')
                                    cinputarguments = [cinputarguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint32')
                                    cinputarguments = [cinputarguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int32')
                                    cinputarguments = [cinputarguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint64')
                                    cinputarguments = [cinputarguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int64')
                                    cinputarguments = [cinputarguments,['''','int_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'boolean')
                                    cinputarguments = [cinputarguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                else
                                    cinputarguments = [cinputarguments,['''',char(nextScreenObj.Workflow.Outputs.(2){i}),' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                end
                            else
                                if strcmp(char(nextScreenObj.Workflow.Outputs.(2){i}),'single')
                                    cinputarguments = [cinputarguments,['''','float',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint8')
                                    cinputarguments = [cinputarguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int8')
                                    cinputarguments = [cinputarguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint16')
                                    cinputarguments = [cinputarguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int16')
                                    cinputarguments = [cinputarguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint32')
                                    cinputarguments = [cinputarguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int32')
                                    cinputarguments = [cinputarguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint64')
                                    cinputarguments = [cinputarguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int64')
                                    cinputarguments = [cinputarguments,['''','int_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'boolean')
                                    cinputarguments = [cinputarguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                else
                                    cinputarguments = [cinputarguments,['''',char(nextScreenObj.Workflow.Outputs.(2){i}),' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                end
                            end

                        else
                            if str2num(char(extractBefore(extractBetween(nextScreenObj.Workflow.Outputs.(3){i},'[',']'),',')))>1
                                if strcmp(char(nextScreenObj.Workflow.Outputs.(2){i}),'single')
                                    cinputarguments = [cinputarguments,['''','float',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+94),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint8')
                                    cinputarguments = [cinputarguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int8')
                                    cinputarguments = [cinputarguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint16')
                                    cinputarguments = [cinputarguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int16')
                                    cinputarguments = [cinputarguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint32')
                                    cinputarguments = [cinputarguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int32')
                                    cinputarguments = [cinputarguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint64')
                                    cinputarguments = [cinputarguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int64')
                                    cinputarguments = [cinputarguments,['''','int_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'boolean')
                                    cinputarguments = [cinputarguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                else
                                    cinputarguments = [cinputarguments,['''',char(nextScreenObj.Workflow.Outputs.(2){i}),' ** ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                end
                            else
                                if strcmp(char(nextScreenObj.Workflow.Outputs.(2){i}),'single')
                                    cinputarguments = [cinputarguments,['''','float',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+94),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint8')
                                    cinputarguments = [cinputarguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int8')
                                    cinputarguments = [cinputarguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint16')
                                    cinputarguments = [cinputarguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int16')
                                    cinputarguments = [cinputarguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint32')
                                    cinputarguments = [cinputarguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int32')
                                    cinputarguments = [cinputarguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint64')
                                    cinputarguments = [cinputarguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int64')
                                    cinputarguments = [cinputarguments,['''','int_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'boolean')
                                    cinputarguments = [cinputarguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                else
                                    cinputarguments = [cinputarguments,['''',char(nextScreenObj.Workflow.Outputs.(2){i}),' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                end
                            end
                        end
                    end
                    if (height(nextScreenObj.Workflow.Outputs) ~= 0 & (height(currentScreen.Workflow.Inputs) ~= 0 ))
                        cinputarguments = [cinputarguments,','];
                    end

                    for i=1:length(currentScreen.Workflow.Inputs{:,1})
                        if i==length(currentScreen.Workflow.Inputs{:,1})
                            if str2num(char(extractBefore(extractBetween(currentScreen.Workflow.Inputs.(3){i},'[',']'),',')))>1
                                if strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'single')
                                    cinputInputarguments = [cinputInputarguments,['''','float',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint8')
                                    cinputInputarguments = [cinputInputarguments,['''','uint8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int8')
                                    cinputInputarguments = [cinputInputarguments,['''','int8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint16')
                                    cinputInputarguments = [cinputInputarguments,['''','uint16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int16')
                                    cinputInputarguments = [cinputInputarguments,['''','int16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint32')
                                    cinputInputarguments = [cinputInputarguments,['''','uint32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int32')
                                    cinputInputarguments = [cinputInputarguments,['''','int32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint64')
                                    cinputInputarguments = [cinputInputarguments,['''','uint_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int64')
                                    cinputInputarguments = [cinputInputarguments,['''','int_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'boolean')
                                    cinputInputarguments = [cinputInputarguments,['''','boolean_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                                else
                                    cinputInputarguments = [cinputInputarguments,['''',char(currentScreen.Workflow.Inputs.(2){i}),' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                                end
                            else
                                if strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'single')
                                    cinputInputarguments = [cinputInputarguments,['''','float',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint8')
                                    cinputInputarguments = [cinputInputarguments,['''','uint8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int8')
                                    cinputInputarguments = [cinputInputarguments,['''','int8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint16')
                                    cinputInputarguments = [cinputInputarguments,['''','uint16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int16')
                                    cinputInputarguments = [cinputInputarguments,['''','int16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint32')
                                    cinputInputarguments = [cinputInputarguments,['''','uint32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int32')
                                    cinputInputarguments = [cinputInputarguments,['''','int32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint64')
                                    cinputInputarguments = [cinputInputarguments,['''','uint_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int64')
                                    cinputInputarguments = [cinputInputarguments,['''','int_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'boolean')
                                    cinputInputarguments = [cinputInputarguments,['''','boolean_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                                else
                                    cinputInputarguments = [cinputInputarguments,['''',char(currentScreen.Workflow.Inputs.(2){i}),' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                                end
                            end
                        else
                            if str2num(char(extractBefore(extractBetween(currentScreen.Workflow.Inputs.(3){i},'[',']'),',')))>1
                                if strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'single')
                                    cinputInputarguments = [cinputInputarguments,['''','float',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint8')
                                    cinputInputarguments = [cinputInputarguments,['''','uint8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int8')
                                    cinputInputarguments = [cinputInputarguments,['''','int8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint16')
                                    cinputInputarguments = [cinputInputarguments,['''','uint16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int16')
                                    cinputInputarguments = [cinputInputarguments,['''','int16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint32')
                                    cinputInputarguments = [cinputInputarguments,['''','uint32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int32')
                                    cinputInputarguments = [cinputInputarguments,['''','int32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint64')
                                    cinputInputarguments = [cinputInputarguments,['''','uint_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int64')
                                    cinputInputarguments = [cinputInputarguments,['''','int_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'boolean')
                                    cinputInputarguments = [cinputInputarguments,['''','boolean_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                                else
                                    cinputInputarguments = [cinputInputarguments,['''',char(currentScreen.Workflow.Inputs.(2){i}),' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                                end
                            else
                                if strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'single')
                                    cinputInputarguments = [cinputInputarguments,['''','float',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint8')
                                    cinputInputarguments = [cinputInputarguments,['''','uint8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int8')
                                    cinputInputarguments = [cinputInputarguments,['''','int8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint16')
                                    cinputInputarguments = [cinputInputarguments,['''','uint16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int16')
                                    cinputInputarguments = [cinputInputarguments,['''','int16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint32')
                                    cinputInputarguments = [cinputInputarguments,['''','uint32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int32')
                                    cinputInputarguments = [cinputInputarguments,['''','int32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint64')
                                    cinputInputarguments = [cinputInputarguments,['''','uint_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int64')
                                    cinputInputarguments = [cinputInputarguments,['''','int_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'boolean')
                                    cinputInputarguments = [cinputInputarguments,['''','boolean_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                                else
                                    cinputInputarguments = [cinputInputarguments,['''',char(currentScreen.Workflow.Inputs.(2){i}),' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                                end
                            end
                        end
                    end
                    if (height(nextScreenObj.Workflow.Outputs) ~= 0 & (height(currentScreen.Workflow.Inputs) ~= 0 ))
                        cinputarguments = ['cInputArgumentstoStepFcn = {',cinputarguments,cinputInputarguments,'};'];
                    elseif height(nextScreenObj.Workflow.Outputs) == 0
                        cinputarguments = ['cInputArgumentstoStepFcn = {',cinputInputarguments,'};'];
                    else
                        cinputarguments = ['cInputArgumentstoStepFcn = {',cinputarguments,'};'];
                    end
                    addInputStepArgs = 'addInputStepArguments(drvObj,cInputArgumentstoStepFcn,mInputArgumentsToCStepFcn);';
                end

                cinputsetuparguments = '';
                addInputSetupArgs = '';
                if (height(nextScreenObj.Workflow.MaskParameters) ~= 0)
                    for i=1:length(nextScreenObj.Workflow.MaskParameters{:,1})
                        if i==length(nextScreenObj.Workflow.MaskParameters{:,1})
                            if str2num(char(extractBefore(extractBetween(currentScreen.Workflow.MaskParameters.(3){i},'[',']'),',')))>1
                                if strcmp(char(nextScreenObj.Workflow.MaskParameters.(2){i}),'single')
                                    cinputsetuparguments = [cinputsetuparguments,['''','float',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'boolean')
                                    cinputsetuparguments = [cinputsetuparguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                else
                                    cinputsetuparguments = [cinputsetuparguments,['''',char(nextScreenObj.Workflow.MaskParameters.(2){i}),' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                end
                            else
                                if strcmp(char(nextScreenObj.Workflow.MaskParameters.(2){i}),'single')
                                    cinputsetuparguments = [cinputsetuparguments,['''','float',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'boolean')
                                    cinputsetuparguments = [cinputsetuparguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                else
                                    cinputsetuparguments = [cinputsetuparguments,['''',char(nextScreenObj.Workflow.MaskParameters.(2){i}),' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                end
                            end
                        else
                            if str2num(char(extractBefore(extractBetween(currentScreen.Workflow.MaskParameters.(3){i},'[',']'),',')))>1
                                if strcmp(char(nextScreenObj.Workflow.MaskParameters.(2){i}),'single')
                                    cinputsetuparguments = [cinputsetuparguments,['''','float',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'boolean')
                                    cinputsetuparguments = [cinputsetuparguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                else
                                    cinputsetuparguments = [cinputsetuparguments,['''',char(nextScreenObj.Workflow.MaskParameters.(2){i}),' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                end
                            else
                                if strcmp(char(nextScreenObj.Workflow.MaskParameters.(2){i}),'single')
                                    cinputsetuparguments = [cinputsetuparguments,['''','float',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'boolean')
                                    cinputsetuparguments = [cinputsetuparguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                else
                                    cinputsetuparguments = [cinputsetuparguments,['''',char(nextScreenObj.Workflow.MaskParameters.(2){i}),' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                end
                            end
                        end
                    end
                    % if (size(nextScreenObj.Workflow.MaskParameters) ~= 0)
                    %     cinputsetuparguments = [cinputsetuparguments,','];
                    % end
                    cinputsetuparguments = ['cInputArgumentstoSetupFcn = {',cinputsetuparguments,'''};'];
                    addInputSetupArgs = 'addInputSetupArguments(drvObj,cInputArgumentstoSetupFcn,mInputArgumentsToCSetupFcn);';
                end

                nextScreenObj.Workflow.processTemplateFile('createDeviceDriverScript.m','ExecutionScript.m',...
                    {'##Block_Name##',['''',nextScreenObj.Workflow.BlockName,''''],...
                    '##Block_Mask_Description##',['''',nextScreenObj.Workflow.BlockDescription,''''], ...
                    '##Block_Type##','''Source''',....
                    '##Headerfile_Path##',headerfilesPath,...
                    '##Sourcefile_Path##',sourcefilesPath,...
                    '##Sourcefiles##',sourcefiles,...
                    '##Output_Declaration##',createoutputs,...
                    '##Addoutput##',staticOutput2,...
                    '##Input_Declaration##',createinputs,...
                    '##Addinput##',staticInput2,...
                    '##Property_Declaration##',createproperty,...
                    '##Addproperty##',staticProperty2,...
                    '##MinputArguments##',minputarguments,...
                    '##CinputArguments##',cinputarguments,...
                    '##MinputSetupArguments##',minputsetuparguments,...
                    '##CinputSetupArguments##',cinputsetuparguments,...
                    '##addStepArguments##',addInputStepArgs,...
                    '##addSetupArguments##',addInputSetupArgs,...
                    '##WorkingDirectory##',workingdirectory,
                    });
                run('ExecutionScript.m');
                open_system(new_system('untitiled789'));
                BlockHandle = ['untitiled789' '/' currentScreen.Workflow.BlockName];
                add_block('simulink/User-Defined Functions/MATLAB System',BlockHandle);
                set_param(['untitiled789','/' currentScreen.Workflow.BlockName],'System',currentScreen.Workflow.BlockName);
                set_param(['untitiled789','/' currentScreen.Workflow.BlockName],'Position',[50 50 300 200]);
                print(['-s','untitiled789'],'-djpeg',fullfile(currentScreen.Workflow.WorkingDir,[currentScreen.Workflow.BlockName,'789.jpg']));
                save_system('untitiled789');
                close_system('untitiled789');
                delete untitiled789.slx;
                delete([currentScreen.Workflow.BlockName , '.m']);
                delete([currentScreen.Workflow.BlockName , '.cpp']);
                delete([currentScreen.Workflow.BlockName , '.h']);
                delete(fullfile(currentScreen.Workflow.WorkingDir,[currentScreen.Workflow.BlockName,'456.jpg']));
            end
            if  (strcmp(class(currentScreen),'BlockParametersScreen'))
                % currentScreen.Workflow.Inputs = currentScreen.Table.Data;
                workingdirectory=['generateSensorBlockAndTemplateHooks(drvObj,''',pwd,''');']
                % workingdirectory=currentScreen.Workflow.WorkingDir;
                FileRows= nextScreenObj.Workflow.FolderPath.Variables;
                headerfilesPath = '';
                for i=1:length(FileRows)
                    headerfilesPath=[headerfilesPath,['addHeaderPath(drvObj,''',FileRows{i},''');'],newline];
                end
                sourcefilesPath = '';
                for i=1:length(FileRows)
                    sourcefilesPath=[sourcefilesPath,['addSourcePath(drvObj,''',FileRows{i},''');'],newline];
                end
                sourcefiles = '';
                for i=1:length(nextScreenObj.Workflow.SrcFiles)
                    sourcefiles=[sourcefiles,['addSourceFile(drvObj,{''',nextScreenObj.Workflow.SrcFiles{i},'''});'],newline];
                end
                createoutputs = '';
                outputNames = '';
                if height(nextScreenObj.Workflow.Outputs) ~= 0
                    for i=1:length(nextScreenObj.Workflow.Outputs{:,1})
                        createoutputs = [createoutputs,['output_',char(i+64),' = createOutput(drvObj,''',char(nextScreenObj.Workflow.Outputs.(1){i}),''',','''Datatype''',',''',char(nextScreenObj.Workflow.Outputs.(2){i}),''',','''Size''',',',char(nextScreenObj.Workflow.Outputs.(3){i}),');'],newline];
                    end
                end
                addoutput = '';
                staticOutput2 = '';
                createinputs = '';
                inputNames = '';
                if height(currentScreen.Workflow.Inputs) ~= 0
                    for i=1:length(currentScreen.Workflow.Inputs{:,1})
                        createinputs = [createinputs,['input_',char(i+64),' = createInput(drvObj,''',char(currentScreen.Workflow.Inputs.(1){i}),''',','''Datatype''',',''',char(currentScreen.Workflow.Inputs.(2){i}),''',','''Size''',',',char(currentScreen.Workflow.Inputs.(3){i}),');'],newline];
                    end
                end
                addinput = '';
                if height(currentScreen.Workflow.Inputs) ~= 0
                    staticinput ='addInput(drvObj,';
                    for i=1:length(currentScreen.Workflow.Inputs{:,1})
                        if i==length(currentScreen.Workflow.Inputs{:,1})
                            addinput = [addinput,['input_',char(i+64)]];
                        else
                            addinput = [addinput,['input_',char(i+64),',']];
                        end
                    end
                    staticInput2 = [staticinput,addinput,');',newline];
                else
                    staticInput2 = '';
                end

                createproperty = '';
                propertyNames = '';
                if height(currentScreen.Workflow.MaskParameters) ~= 0
                    for i=1:length(currentScreen.Workflow.MaskParameters{:,1})
                        if strcmp(char(currentScreen.Workflow.MaskParameters.(5){i}),'Non tunable')
                            booleanValue = 'false';
                        else
                            booleanValue = 'true';
                        end
                        createproperty = [createproperty,['property_',char(i+64),' = createProperty(drvObj,''',char(currentScreen.Workflow.MaskParameters.(1){i}),''',','''Datatype''',',''',char(currentScreen.Workflow.MaskParameters.(2){i}),''',','''Size''',',',char(currentScreen.Workflow.MaskParameters.(3){i}),',','''InitValue''',',',currentScreen.Workflow.MaskParameters.(4){i},',','''Tunable''',',',booleanValue,');'],newline];
                    end
                end
                addproperty = '';
                if height(currentScreen.Workflow.MaskParameters) ~= 0
                    staticproperty ='addProperty(drvObj,';
                    for i=1:length(currentScreen.Workflow.MaskParameters{:,1})
                        if i==length(currentScreen.Workflow.MaskParameters{:,1})
                            addproperty = [addproperty,['property_',char(i+64)]];
                        else
                            addproperty = [addproperty,['property_',char(i+64),',']];
                        end
                    end
                    staticProperty2 = [staticproperty,addproperty,');',newline];
                else
                    staticProperty2 = '';
                end


                minputarguments='';
                minputInputarguments='';
                % if ((height(nextScreenObj.Workflow.Outputs) ~= 0) | (height(currentScreen.Workflow.Inputs) ~= 0 ))
                %     for i=1:length(nextScreenObj.Workflow.Outputs{:,1})
                %         if i==length(nextScreenObj.Workflow.Outputs{:,1})
                %             minputarguments = [minputarguments,['output_',char(i+64)]];
                %         else
                %             minputarguments = [minputarguments,['output_',char(i+64),',']];
                %         end
                %     end
                %     if (height(nextScreenObj.Workflow.Outputs) ~= 0 & (height(currentScreen.Workflow.Inputs) ~= 0 ))
                %         minputarguments = [minputarguments,','];
                %     end
                %     for i=1:length(currentScreen.Workflow.Inputs{:,1})
                %         if i==length(currentScreen.Workflow.Inputs{:,1})
                %             minputInputarguments = [minputInputarguments,['input_',char(i+64)]];
                %         else
                %             minputInputarguments = [minputInputarguments,['input_',char(i+64),',']];
                %         end
                %     end
                    % minputarguments=['mInputArgumentsToCStepFcn = {',minputarguments,minputInputarguments,'}'];
                % end
                minputarguments=['mInputArgumentsToCStepFcn = {}'];
                minputsetuparguments='';
                if (height(nextScreenObj.Workflow.MaskParameters) ~= 0)
                    for i=1:length(nextScreenObj.Workflow.MaskParameters{:,1})
                        if i==length(nextScreenObj.Workflow.MaskParameters{:,1})
                            minputsetuparguments = [minputsetuparguments,['property_',char(i+64)]];
                        else
                            minputsetuparguments = [minputsetuparguments,['property_',char(i+64),',']];
                        end
                    end
                    % if (size(nextScreenObj.Workflow.MaskParameters) ~= 0)
                    %     minputsetuparguments = [minputsetuparguments,','];
                    % end

                    minputsetuparguments=['mInputArgumentsToCSetupFcn = {',minputsetuparguments,'}'];
                end

                cinputarguments = '';
                cinputInputarguments = '';
                addInputStepArgs = '';
                % if ((height(nextScreenObj.Workflow.Outputs) ~= 0) | (height(currentScreen.Workflow.Inputs) ~= 0 ))
                %     for i=1:length(nextScreenObj.Workflow.Outputs{:,1})
                %         if i==length(nextScreenObj.Workflow.Outputs{:,1})
                %             if str2num(char(extractBefore(extractBetween(nextScreenObj.Workflow.Outputs.(3){i},'[',']'),',')))>1
                %                 if strcmp(char(nextScreenObj.Workflow.Outputs.(2){i}),'single')
                %                     cinputarguments = [cinputarguments,['''','float',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint8')
                %                     cinputarguments = [cinputarguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int8')
                %                     cinputarguments = [cinputarguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint16')
                %                     cinputarguments = [cinputarguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int16')
                %                     cinputarguments = [cinputarguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint32')
                %                     cinputarguments = [cinputarguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int32')
                %                     cinputarguments = [cinputarguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint64')
                %                     cinputarguments = [cinputarguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int64')
                %                     cinputarguments = [cinputarguments,['''','int_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'boolean')
                %                     cinputarguments = [cinputarguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                %                 else
                %                     cinputarguments = [cinputarguments,['''',char(nextScreenObj.Workflow.Outputs.(2){i}),' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                %                 end
                %             else
                %                 if strcmp(char(nextScreenObj.Workflow.Outputs.(2){i}),'single')
                %                     cinputarguments = [cinputarguments,['''','float',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint8')
                %                     cinputarguments = [cinputarguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int8')
                %                     cinputarguments = [cinputarguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint16')
                %                     cinputarguments = [cinputarguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int16')
                %                     cinputarguments = [cinputarguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint32')
                %                     cinputarguments = [cinputarguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int32')
                %                     cinputarguments = [cinputarguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint64')
                %                     cinputarguments = [cinputarguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int64')
                %                     cinputarguments = [cinputarguments,['''','int_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'boolean')
                %                     cinputarguments = [cinputarguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                %                 else
                %                     cinputarguments = [cinputarguments,['''',char(nextScreenObj.Workflow.Outputs.(2){i}),' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                %                 end
                %             end
                % 
                %         else
                %             if str2num(char(extractBefore(extractBetween(nextScreenObj.Workflow.Outputs.(3){i},'[',']'),',')))>1
                %                 if strcmp(char(nextScreenObj.Workflow.Outputs.(2){i}),'single')
                %                     cinputarguments = [cinputarguments,['''','float',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+94),''','''],['int size_',char(i+64),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint8')
                %                     cinputarguments = [cinputarguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int8')
                %                     cinputarguments = [cinputarguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint16')
                %                     cinputarguments = [cinputarguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int16')
                %                     cinputarguments = [cinputarguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint32')
                %                     cinputarguments = [cinputarguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int32')
                %                     cinputarguments = [cinputarguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint64')
                %                     cinputarguments = [cinputarguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int64')
                %                     cinputarguments = [cinputarguments,['''','int_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'boolean')
                %                     cinputarguments = [cinputarguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                %                 else
                %                     cinputarguments = [cinputarguments,['''',char(nextScreenObj.Workflow.Outputs.(2){i}),' ** ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                %                 end
                %             else
                %                 if strcmp(char(nextScreenObj.Workflow.Outputs.(2){i}),'single')
                %                     cinputarguments = [cinputarguments,['''','float',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+94),''','''],['int size_',char(i+64),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint8')
                %                     cinputarguments = [cinputarguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int8')
                %                     cinputarguments = [cinputarguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint16')
                %                     cinputarguments = [cinputarguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int16')
                %                     cinputarguments = [cinputarguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint32')
                %                     cinputarguments = [cinputarguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int32')
                %                     cinputarguments = [cinputarguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint64')
                %                     cinputarguments = [cinputarguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int64')
                %                     cinputarguments = [cinputarguments,['''','int_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'boolean')
                %                     cinputarguments = [cinputarguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                %                 else
                %                     cinputarguments = [cinputarguments,['''',char(nextScreenObj.Workflow.Outputs.(2){i}),' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                %                 end
                %             end
                %         end
                %     end
                %     if (height(nextScreenObj.Workflow.Outputs) ~= 0 & (height(currentScreen.Workflow.Inputs) ~= 0 ))
                %         cinputarguments = [cinputarguments,','];
                %     end
                % 
                %     for i=1:length(currentScreen.Workflow.Inputs{:,1})
                %         if i==length(currentScreen.Workflow.Inputs{:,1})
                %             if str2num(char(extractBefore(extractBetween(currentScreen.Workflow.Inputs.(3){i},'[',']'),',')))>1
                %                 if strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'single')
                %                     cinputInputarguments = [cinputInputarguments,['''','float',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint8')
                %                     cinputInputarguments = [cinputInputarguments,['''','uint8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int8')
                %                     cinputInputarguments = [cinputInputarguments,['''','int8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint16')
                %                     cinputInputarguments = [cinputInputarguments,['''','uint16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int16')
                %                     cinputInputarguments = [cinputInputarguments,['''','int16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint32')
                %                     cinputInputarguments = [cinputInputarguments,['''','uint32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int32')
                %                     cinputInputarguments = [cinputInputarguments,['''','int32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint64')
                %                     cinputInputarguments = [cinputInputarguments,['''','uint_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int64')
                %                     cinputInputarguments = [cinputInputarguments,['''','int_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'boolean')
                %                     cinputInputarguments = [cinputInputarguments,['''','boolean_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                %                 else
                %                     cinputInputarguments = [cinputInputarguments,['''',char(currentScreen.Workflow.Inputs.(2){i}),' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                %                 end
                %             else
                %                 if strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'single')
                %                     cinputInputarguments = [cinputInputarguments,['''','float',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint8')
                %                     cinputInputarguments = [cinputInputarguments,['''','uint8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int8')
                %                     cinputInputarguments = [cinputInputarguments,['''','int8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint16')
                %                     cinputInputarguments = [cinputInputarguments,['''','uint16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int16')
                %                     cinputInputarguments = [cinputInputarguments,['''','int16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint32')
                %                     cinputInputarguments = [cinputInputarguments,['''','uint32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int32')
                %                     cinputInputarguments = [cinputInputarguments,['''','int32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint64')
                %                     cinputInputarguments = [cinputInputarguments,['''','uint_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int64')
                %                     cinputInputarguments = [cinputInputarguments,['''','int_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'boolean')
                %                     cinputInputarguments = [cinputInputarguments,['''','boolean_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                %                 else
                %                     cinputInputarguments = [cinputInputarguments,['''',char(currentScreen.Workflow.Inputs.(2){i}),' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                %                 end
                %             end
                %         else
                %             if str2num(char(extractBefore(extractBetween(currentScreen.Workflow.Inputs.(3){i},'[',']'),',')))>1
                %                 if strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'single')
                %                     cinputInputarguments = [cinputInputarguments,['''','float',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint8')
                %                     cinputInputarguments = [cinputInputarguments,['''','uint8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int8')
                %                     cinputInputarguments = [cinputInputarguments,['''','int8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint16')
                %                     cinputInputarguments = [cinputInputarguments,['''','uint16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int16')
                %                     cinputInputarguments = [cinputInputarguments,['''','int16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint32')
                %                     cinputInputarguments = [cinputInputarguments,['''','uint32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int32')
                %                     cinputInputarguments = [cinputInputarguments,['''','int32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint64')
                %                     cinputInputarguments = [cinputInputarguments,['''','uint_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int64')
                %                     cinputInputarguments = [cinputInputarguments,['''','int_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'boolean')
                %                     cinputInputarguments = [cinputInputarguments,['''','boolean_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                %                 else
                %                     cinputInputarguments = [cinputInputarguments,['''',char(currentScreen.Workflow.Inputs.(2){i}),' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                %                 end
                %             else
                %                 if strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'single')
                %                     cinputInputarguments = [cinputInputarguments,['''','float',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint8')
                %                     cinputInputarguments = [cinputInputarguments,['''','uint8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int8')
                %                     cinputInputarguments = [cinputInputarguments,['''','int8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint16')
                %                     cinputInputarguments = [cinputInputarguments,['''','uint16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int16')
                %                     cinputInputarguments = [cinputInputarguments,['''','int16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint32')
                %                     cinputInputarguments = [cinputInputarguments,['''','uint32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int32')
                %                     cinputInputarguments = [cinputInputarguments,['''','int32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint64')
                %                     cinputInputarguments = [cinputInputarguments,['''','uint_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int64')
                %                     cinputInputarguments = [cinputInputarguments,['''','int_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                %                 elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'boolean')
                %                     cinputInputarguments = [cinputInputarguments,['''','boolean_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                %                 else
                %                     cinputInputarguments = [cinputInputarguments,['''',char(currentScreen.Workflow.Inputs.(2){i}),' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                %                 end
                %             end
                %         end
                %     end
                %     if (height(nextScreenObj.Workflow.Outputs) ~= 0 & (height(currentScreen.Workflow.Inputs) ~= 0 ))
                %         cinputarguments = ['cInputArgumentstoStepFcn = {',cinputarguments,cinputInputarguments,'};'];
                %     elseif height(nextScreenObj.Workflow.Outputs) == 0
                %         cinputarguments = ['cInputArgumentstoStepFcn = {',cinputInputarguments,'};'];
                %     else
                %         cinputarguments = ['cInputArgumentstoStepFcn = {',cinputarguments,'};'];
                %     end
                %     addInputStepArgs = 'addInputStepArguments(drvObj,cInputArgumentstoStepFcn,mInputArgumentsToCStepFcn);';
                % end
                cinputarguments = 'cInputArgumentstoStepFcn = {}';
                addInputStepArgs = 'addInputStepArguments(drvObj,cInputArgumentstoStepFcn,mInputArgumentsToCStepFcn);';
                cinputsetuparguments = '';
                addInputSetupArgs = '';
                if (height(nextScreenObj.Workflow.MaskParameters) ~= 0)
                    for i=1:length(nextScreenObj.Workflow.MaskParameters{:,1})
                        if i==length(nextScreenObj.Workflow.MaskParameters{:,1})
                            if str2num(char(extractBefore(extractBetween(currentScreen.Workflow.MaskParameters.(3){i},'[',']'),',')))>1
                                if strcmp(char(nextScreenObj.Workflow.MaskParameters.(2){i}),'single')
                                    cinputsetuparguments = [cinputsetuparguments,['''','float',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'boolean')
                                    cinputsetuparguments = [cinputsetuparguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                else
                                    cinputsetuparguments = [cinputsetuparguments,['''',char(nextScreenObj.Workflow.MaskParameters.(2){i}),' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                end
                            else
                                if strcmp(char(nextScreenObj.Workflow.MaskParameters.(2){i}),'single')
                                    cinputsetuparguments = [cinputsetuparguments,['''','float',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'boolean')
                                    cinputsetuparguments = [cinputsetuparguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                else
                                    cinputsetuparguments = [cinputsetuparguments,['''',char(nextScreenObj.Workflow.MaskParameters.(2){i}),' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                end
                            end
                        else
                            if str2num(char(extractBefore(extractBetween(currentScreen.Workflow.MaskParameters.(3){i},'[',']'),',')))>1
                                if strcmp(char(nextScreenObj.Workflow.MaskParameters.(2){i}),'single')
                                    cinputsetuparguments = [cinputsetuparguments,['''','float',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'boolean')
                                    cinputsetuparguments = [cinputsetuparguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                else
                                    cinputsetuparguments = [cinputsetuparguments,['''',char(nextScreenObj.Workflow.MaskParameters.(2){i}),' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                end
                            else
                                if strcmp(char(nextScreenObj.Workflow.MaskParameters.(2){i}),'single')
                                    cinputsetuparguments = [cinputsetuparguments,['''','float',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'boolean')
                                    cinputsetuparguments = [cinputsetuparguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                else
                                    cinputsetuparguments = [cinputsetuparguments,['''',char(nextScreenObj.Workflow.MaskParameters.(2){i}),' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                end
                            end
                        end
                    end
                    % if (size(nextScreenObj.Workflow.MaskParameters) ~= 0)
                    %     cinputsetuparguments = [cinputsetuparguments,','];
                    % end
                    cinputsetuparguments = ['cInputArgumentstoSetupFcn = {',cinputsetuparguments,'''};'];
                    addInputSetupArgs = 'addInputSetupArguments(drvObj,cInputArgumentstoSetupFcn,mInputArgumentsToCSetupFcn);';
                end

                nextScreenObj.Workflow.processTemplateFile('createDeviceDriverScript.m','ExecutionScript.m',...
                    {'##Block_Name##',['''',nextScreenObj.Workflow.BlockName,''''],...
                    '##Block_Mask_Description##',['''',nextScreenObj.Workflow.BlockDescription,''''], ...
                    '##Block_Type##','''Source''',....
                    '##Headerfile_Path##',headerfilesPath,...
                    '##Sourcefile_Path##',sourcefilesPath,...
                    '##Sourcefiles##',sourcefiles,...
                    '##Output_Declaration##',createoutputs,...
                    '##Addoutput##',staticOutput2,...
                    '##Input_Declaration##',createinputs,...
                    '##Addinput##',staticInput2,...
                    '##Property_Declaration##',createproperty,...
                    '##Addproperty##',staticProperty2,...
                    '##MinputArguments##',minputarguments,...
                    '##CinputArguments##',cinputarguments,...
                    '##MinputSetupArguments##',minputsetuparguments,...
                    '##CinputSetupArguments##',cinputsetuparguments,...
                    '##addStepArguments##',addInputStepArgs,...
                    '##addSetupArguments##',addInputSetupArgs,...
                    '##WorkingDirectory##',workingdirectory,
                    });
                run('ExecutionScript.m');
                open_system(new_system('untitiled123'));
                BlockHandle = ['untitiled123' '/' currentScreen.Workflow.BlockName];
                add_block('simulink/User-Defined Functions/MATLAB System',BlockHandle);
                set_param(['untitiled123','/' currentScreen.Workflow.BlockName],'System',currentScreen.Workflow.BlockName);
                set_param(['untitiled123','/' currentScreen.Workflow.BlockName],'Position',[50 50 300 200]);
                print(['-s','untitiled123'],'-djpeg',fullfile(currentScreen.Workflow.WorkingDir,[currentScreen.Workflow.BlockName,'123.jpg']));
                save_system('untitiled123');
                close_system('untitiled123');
                delete 'untitiled123.slx';
                delete([currentScreen.Workflow.BlockName , '.m']);
                delete([currentScreen.Workflow.BlockName , '.cpp']);
                delete([currentScreen.Workflow.BlockName , '.h']);
            end
                if (strcmp(class(currentScreen),'OutputScreen')) 
                currentScreen.Workflow.Outputs = currentScreen.Table.Data;

                workingdirectory=['generateSensorBlockAndTemplateHooks(drvObj,''',pwd,''');']
                % workingdirectory=currentScreen.Workflow.WorkingDir;
                FileRows= nextScreenObj.Workflow.FolderPath.Variables;
                headerfilesPath = '';
                for i=1:length(FileRows)
                    headerfilesPath=[headerfilesPath,['addHeaderPath(drvObj,''',FileRows{i},''');'],newline];
                end
                sourcefilesPath = '';
                for i=1:length(FileRows)
                    sourcefilesPath=[sourcefilesPath,['addSourcePath(drvObj,''',FileRows{i},''');'],newline];
                end
                sourcefiles = '';
                for i=1:length(nextScreenObj.Workflow.SrcFiles)
                    sourcefiles=[sourcefiles,['addSourceFile(drvObj,{''',nextScreenObj.Workflow.SrcFiles{i},'''});'],newline];
                end
                createoutputs = '';
                outputNames = '';
                if height(nextScreenObj.Workflow.Outputs) ~= 0
                    for i=1:length(nextScreenObj.Workflow.Outputs{:,1})
                        createoutputs = [createoutputs,['output_',char(i+64),' = createOutput(drvObj,''',char(nextScreenObj.Workflow.Outputs.(1){i}),''',','''Datatype''',',''',char(nextScreenObj.Workflow.Outputs.(2){i}),''',','''Size''',',',char(nextScreenObj.Workflow.Outputs.(3){i}),');'],newline];
                    end
                end
                addoutput = '';
                if height(nextScreenObj.Workflow.Outputs) ~= 0
                    staticoutput ='addOutput(drvObj,';
                    for i=1:length(nextScreenObj.Workflow.Outputs{:,1})
                        if i==length(nextScreenObj.Workflow.Outputs{:,1})
                            addoutput = [addoutput,['output_',char(i+64)]];
                        else
                            addoutput = [addoutput,['output_',char(i+64),',']];
                        end
                    end
                    staticOutput2 = [staticoutput,addoutput,');',newline];
                else
                    staticOutput2 = '';
                end

                createinputs = '';
                inputNames = '';
                if height(currentScreen.Workflow.Inputs) ~= 0
                    for i=1:length(currentScreen.Workflow.Inputs{:,1})
                        createinputs = [createinputs,['input_',char(i+64),' = createInput(drvObj,''',char(currentScreen.Workflow.Inputs.(1){i}),''',','''Datatype''',',''',char(currentScreen.Workflow.Inputs.(2){i}),''',','''Size''',',',char(currentScreen.Workflow.Inputs.(3){i}),');'],newline];
                    end
                end
                addinput = '';
                if height(currentScreen.Workflow.Inputs) ~= 0
                    staticinput ='addInput(drvObj,';
                    for i=1:length(currentScreen.Workflow.Inputs{:,1})
                        if i==length(currentScreen.Workflow.Inputs{:,1})
                            addinput = [addinput,['input_',char(i+64)]];
                        else
                            addinput = [addinput,['input_',char(i+64),',']];
                        end
                    end
                    staticInput2 = [staticinput,addinput,');',newline];
                else
                    staticInput2 = '';
                end

                createproperty = '';
                propertyNames = '';
                if height(currentScreen.Workflow.MaskParameters) ~= 0
                    for i=1:length(currentScreen.Workflow.MaskParameters{:,1})
                        if strcmp(char(currentScreen.Workflow.MaskParameters.(5){i}),'Non tunable')
                            booleanValue = 'false';
                        else
                            booleanValue = 'true';
                        end
                        createproperty = [createproperty,['property_',char(i+64),' = createProperty(drvObj,''',char(currentScreen.Workflow.MaskParameters.(1){i}),''',','''Datatype''',',''',char(currentScreen.Workflow.MaskParameters.(2){i}),''',','''Size''',',',char(currentScreen.Workflow.MaskParameters.(3){i}),',','''InitValue''',',',currentScreen.Workflow.MaskParameters.(4){i},',','''Tunable''',',',booleanValue,');'],newline];
                    end
                end
                addproperty = '';
                if height(currentScreen.Workflow.MaskParameters) ~= 0
                    staticproperty ='addProperty(drvObj,';
                    for i=1:length(currentScreen.Workflow.MaskParameters{:,1})
                        if i==length(currentScreen.Workflow.MaskParameters{:,1})
                            addproperty = [addproperty,['property_',char(i+64)]];
                        else
                            addproperty = [addproperty,['property_',char(i+64),',']];
                        end
                    end
                    staticProperty2 = [staticproperty,addproperty,');',newline];
                else
                    staticProperty2 = '';
                end


                minputarguments='';
                minputInputarguments='';
                if ((height(nextScreenObj.Workflow.Outputs) ~= 0) | (height(currentScreen.Workflow.Inputs) ~= 0 ))
                    for i=1:length(nextScreenObj.Workflow.Outputs{:,1})
                        if i==length(nextScreenObj.Workflow.Outputs{:,1})
                            minputarguments = [minputarguments,['output_',char(i+64)]];
                        else
                            minputarguments = [minputarguments,['output_',char(i+64),',']];
                        end
                    end
                    
                   
                    minputarguments=['mInputArgumentsToCStepFcn = {',minputarguments,'}'];
                end

                minputsetuparguments='';
                if (height(nextScreenObj.Workflow.MaskParameters) ~= 0)
                    for i=1:length(nextScreenObj.Workflow.MaskParameters{:,1})
                        if i==length(nextScreenObj.Workflow.MaskParameters{:,1})
                            minputsetuparguments = [minputsetuparguments,['property_',char(i+64)]];
                        else
                            minputsetuparguments = [minputsetuparguments,['property_',char(i+64),',']];
                        end
                    end
                    % if (size(nextScreenObj.Workflow.MaskParameters) ~= 0)
                    %     minputsetuparguments = [minputsetuparguments,','];
                    % end

                    minputsetuparguments=['mInputArgumentsToCSetupFcn = {',minputsetuparguments,'}'];
                end

                cinputarguments = '';
                cinputInputarguments = '';
                addInputStepArgs = '';
                if ((height(nextScreenObj.Workflow.Outputs) ~= 0) | (height(currentScreen.Workflow.Inputs) ~= 0 ))
                    for i=1:length(nextScreenObj.Workflow.Outputs{:,1})
                        if i==length(nextScreenObj.Workflow.Outputs{:,1})
                            if str2num(char(extractBefore(extractBetween(nextScreenObj.Workflow.Outputs.(3){i},'[',']'),',')))>1
                                if strcmp(char(nextScreenObj.Workflow.Outputs.(2){i}),'single')
                                    cinputarguments = [cinputarguments,['''','float',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint8')
                                    cinputarguments = [cinputarguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int8')
                                    cinputarguments = [cinputarguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint16')
                                    cinputarguments = [cinputarguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int16')
                                    cinputarguments = [cinputarguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint32')
                                    cinputarguments = [cinputarguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int32')
                                    cinputarguments = [cinputarguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint64')
                                    cinputarguments = [cinputarguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int64')
                                    cinputarguments = [cinputarguments,['''','int_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'boolean')
                                    cinputarguments = [cinputarguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                else
                                    cinputarguments = [cinputarguments,['''',char(nextScreenObj.Workflow.Outputs.(2){i}),' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                end
                            else
                                if strcmp(char(nextScreenObj.Workflow.Outputs.(2){i}),'single')
                                    cinputarguments = [cinputarguments,['''','float',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint8')
                                    cinputarguments = [cinputarguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int8')
                                    cinputarguments = [cinputarguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint16')
                                    cinputarguments = [cinputarguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int16')
                                    cinputarguments = [cinputarguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint32')
                                    cinputarguments = [cinputarguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int32')
                                    cinputarguments = [cinputarguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint64')
                                    cinputarguments = [cinputarguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int64')
                                    cinputarguments = [cinputarguments,['''','int_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'boolean')
                                    cinputarguments = [cinputarguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                else
                                    cinputarguments = [cinputarguments,['''',char(nextScreenObj.Workflow.Outputs.(2){i}),' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                end
                            end

                        else
                            if str2num(char(extractBefore(extractBetween(nextScreenObj.Workflow.Outputs.(3){i},'[',']'),',')))>1
                                if strcmp(char(nextScreenObj.Workflow.Outputs.(2){i}),'single')
                                    cinputarguments = [cinputarguments,['''','float',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+94),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint8')
                                    cinputarguments = [cinputarguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int8')
                                    cinputarguments = [cinputarguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint16')
                                    cinputarguments = [cinputarguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int16')
                                    cinputarguments = [cinputarguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint32')
                                    cinputarguments = [cinputarguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int32')
                                    cinputarguments = [cinputarguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint64')
                                    cinputarguments = [cinputarguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int64')
                                    cinputarguments = [cinputarguments,['''','int_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'boolean')
                                    cinputarguments = [cinputarguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                else
                                    cinputarguments = [cinputarguments,['''',char(nextScreenObj.Workflow.Outputs.(2){i}),' ** ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                end
                            else
                                if strcmp(char(nextScreenObj.Workflow.Outputs.(2){i}),'single')
                                    cinputarguments = [cinputarguments,['''','float',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+94),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint8')
                                    cinputarguments = [cinputarguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int8')
                                    cinputarguments = [cinputarguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint16')
                                    cinputarguments = [cinputarguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int16')
                                    cinputarguments = [cinputarguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint32')
                                    cinputarguments = [cinputarguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int32')
                                    cinputarguments = [cinputarguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint64')
                                    cinputarguments = [cinputarguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int64')
                                    cinputarguments = [cinputarguments,['''','int_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'boolean')
                                    cinputarguments = [cinputarguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                else
                                    cinputarguments = [cinputarguments,['''',char(nextScreenObj.Workflow.Outputs.(2){i}),' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                end
                            end
                        end
                    end                  
                    if (height(nextScreenObj.Workflow.Outputs) ~= 0 )
                        cinputarguments = ['cInputArgumentstoStepFcn = {',cinputarguments,cinputInputarguments,'};'];
                    elseif height(nextScreenObj.Workflow.Outputs) == 0
                        cinputarguments = ['cInputArgumentstoStepFcn = {',cinputInputarguments,'};'];
                    else
                        cinputarguments = ['cInputArgumentstoStepFcn = {',cinputarguments,'};'];
                    end
                    addInputStepArgs = 'addInputStepArguments(drvObj,cInputArgumentstoStepFcn,mInputArgumentsToCStepFcn);';
                end

                cinputsetuparguments = '';
                addInputSetupArgs = '';
                if (height(nextScreenObj.Workflow.MaskParameters) ~= 0)
                    for i=1:length(nextScreenObj.Workflow.MaskParameters{:,1})
                        if i==length(nextScreenObj.Workflow.MaskParameters{:,1})
                            if str2num(char(extractBefore(extractBetween(currentScreen.Workflow.MaskParameters.(3){i},'[',']'),',')))>1
                                if strcmp(char(nextScreenObj.Workflow.MaskParameters.(2){i}),'single')
                                    cinputsetuparguments = [cinputsetuparguments,['''','float',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'boolean')
                                    cinputsetuparguments = [cinputsetuparguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                else
                                    cinputsetuparguments = [cinputsetuparguments,['''',char(nextScreenObj.Workflow.MaskParameters.(2){i}),' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                end
                            else
                                if strcmp(char(nextScreenObj.Workflow.MaskParameters.(2){i}),'single')
                                    cinputsetuparguments = [cinputsetuparguments,['''','float',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'boolean')
                                    cinputsetuparguments = [cinputsetuparguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                else
                                    cinputsetuparguments = [cinputsetuparguments,['''',char(nextScreenObj.Workflow.MaskParameters.(2){i}),' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                end
                            end
                        else
                            if str2num(char(extractBefore(extractBetween(currentScreen.Workflow.MaskParameters.(3){i},'[',']'),',')))>1
                                if strcmp(char(nextScreenObj.Workflow.MaskParameters.(2){i}),'single')
                                    cinputsetuparguments = [cinputsetuparguments,['''','float',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'boolean')
                                    cinputsetuparguments = [cinputsetuparguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                else
                                    cinputsetuparguments = [cinputsetuparguments,['''',char(nextScreenObj.Workflow.MaskParameters.(2){i}),' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                end
                            else
                                if strcmp(char(nextScreenObj.Workflow.MaskParameters.(2){i}),'single')
                                    cinputsetuparguments = [cinputsetuparguments,['''','float',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'boolean')
                                    cinputsetuparguments = [cinputsetuparguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                else
                                    cinputsetuparguments = [cinputsetuparguments,['''',char(nextScreenObj.Workflow.MaskParameters.(2){i}),' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                end
                            end
                        end
                    end
                    % if (size(nextScreenObj.Workflow.MaskParameters) ~= 0)
                    %     cinputsetuparguments = [cinputsetuparguments,','];
                    % end
                    cinputsetuparguments = ['cInputArgumentstoSetupFcn = {',cinputsetuparguments,'''};'];
                    addInputSetupArgs = 'addInputSetupArguments(drvObj,cInputArgumentstoSetupFcn,mInputArgumentsToCSetupFcn);';
                end

                nextScreenObj.Workflow.processTemplateFile('createDeviceDriverScript.m','ExecutionScript.m',...
                    {'##Block_Name##',['''',nextScreenObj.Workflow.BlockName,''''],...
                    '##Block_Mask_Description##',['''',nextScreenObj.Workflow.BlockDescription,''''], ...
                    '##Block_Type##','''Source''',....
                    '##Headerfile_Path##',headerfilesPath,...
                    '##Sourcefile_Path##',sourcefilesPath,...
                    '##Sourcefiles##',sourcefiles,...
                    '##Output_Declaration##',createoutputs,...
                    '##Addoutput##',staticOutput2,...
                    '##Input_Declaration##',createinputs,...
                    '##Addinput##',staticInput2,...
                    '##Property_Declaration##',createproperty,...
                    '##Addproperty##',staticProperty2,...
                    '##MinputArguments##',minputarguments,...
                    '##CinputArguments##',cinputarguments,...
                    '##MinputSetupArguments##',minputsetuparguments,...
                    '##CinputSetupArguments##',cinputsetuparguments,...
                    '##addStepArguments##',addInputStepArgs,...
                    '##addSetupArguments##',addInputSetupArgs,...
                    '##WorkingDirectory##',workingdirectory,
                    });
                run('ExecutionScript.m');
                open_system(new_system('untitiled456'));
                BlockHandle = ['untitiled456' '/' currentScreen.Workflow.BlockName];
                add_block('simulink/User-Defined Functions/MATLAB System',BlockHandle);
                set_param(['untitiled456','/' currentScreen.Workflow.BlockName],'System',currentScreen.Workflow.BlockName);
                set_param(['untitiled456','/' currentScreen.Workflow.BlockName],'Position',[50 50 300 200]);
                print(['-s','untitiled456'],'-djpeg',fullfile(currentScreen.Workflow.WorkingDir,[currentScreen.Workflow.BlockName,'456.jpg']));
                save_system('untitiled456');
                close_system('untitiled456');
                delete 'untitiled456.slx';
                delete(fullfile(currentScreen.Workflow.WorkingDir,[currentScreen.Workflow.BlockName,'123.jpg']));
            end
            if (strcmp(class(nextScreenObj),'PreviewScreen'))
                nextScreenObj.BlockImage.ImageFile = fullfile(currentScreen.Workflow.WorkingDir,[currentScreen.Workflow.BlockName,'789.jpg']);
            end
            if (strcmp(class(nextScreenObj),'InputScreen'))
                nextScreenObj.BlockImageInput.ImageFile = fullfile(currentScreen.Workflow.WorkingDir,[currentScreen.Workflow.BlockName,'456.jpg']);
            end
            if (strcmp(class(nextScreenObj),'OutputScreen'))
                nextScreenObj.BlockImageOutput.ImageFile = fullfile(currentScreen.Workflow.WorkingDir,[currentScreen.Workflow.BlockName,'123.jpg']);
            end
            if (strcmp(class(nextScreenObj),'SelectSourceFilesScreen')) 
                currentScreen.Workflow.FolderPath = currentScreen.Table.Data;
                % currentScreen.Workflow.CurrentDirectory
                FilesList={};
                if height(currentScreen.Workflow.FolderPath)
                    FileRows= currentScreen.Workflow.FolderPath.Variables;

                    for i=1:length(FileRows)
                        files=dir(FileRows{i});
                        for j=1:length(files)
                            if(contains(files(j).name,'.cpp') | contains(files(j).name,'.c'))
                                FilesList =[FilesList,fullfile(FileRows{i},files(j).name)];
                            end
                            %FilesList =[FilesList,fullfile(files(j).name)];
                        end
                    end
                end
                nextScreenObj.CheckBoxList.Items=FilesList;
            end

             % Blockname = '';
            if (strcmp(class(currentScreen),'BlockParametersScreen')) 
                currentScreen.Workflow.MaskParameters = currentScreen.Table.Data;
                % Blockname=currentScreen.Workflow.BlockName;
            end

            if (strcmp(class(nextScreenObj),'FileGenerationScreen')) 
                nextScreenObj.FolderTextLabel.Text= nextScreenObj.Workflow.WorkingDir;
            end
            if (strcmp(class(currentScreen),'FileGenerationScreen')) 
                workingdirectory=['generateSensorBlockAndTemplateHooks(drvObj,''',currentScreen.Workflow.WorkingDir,''');']
                % workingdirectory=currentScreen.Workflow.WorkingDir;
                FileRows= nextScreenObj.Workflow.FolderPath.Variables;
                headerfilesPath = '';
                for i=1:length(FileRows)
                    headerfilesPath=[headerfilesPath,['addHeaderPath(drvObj,''',FileRows{i},''');'],newline];
                end
                sourcefilesPath = '';
                for i=1:length(FileRows)
                    sourcefilesPath=[sourcefilesPath,['addSourcePath(drvObj,''',FileRows{i},''');'],newline];
                end
                sourcefiles = '';
                for i=1:length(nextScreenObj.Workflow.SrcFiles)
                    sourcefiles=[sourcefiles,['addSourceFile(drvObj,{''',nextScreenObj.Workflow.SrcFiles{i},'''});'],newline];
                end
                createoutputs = '';
                outputNames = '';
                if height(nextScreenObj.Workflow.Outputs) ~= 0
                    for i=1:length(nextScreenObj.Workflow.Outputs{:,1})
                        createoutputs = [createoutputs,['output_',char(i+64),' = createOutput(drvObj,''',char(nextScreenObj.Workflow.Outputs.(1){i}),''',','''Datatype''',',''',char(nextScreenObj.Workflow.Outputs.(2){i}),''',','''Size''',',',char(nextScreenObj.Workflow.Outputs.(3){i}),');'],newline];
                    end
                end
                addoutput = '';
                if height(nextScreenObj.Workflow.Outputs) ~= 0
                    staticoutput ='addOutput(drvObj,';
                    for i=1:length(nextScreenObj.Workflow.Outputs{:,1})
                        if i==length(nextScreenObj.Workflow.Outputs{:,1})
                            addoutput = [addoutput,['output_',char(i+64)]];
                        else
                            addoutput = [addoutput,['output_',char(i+64),',']];
                        end
                    end
                    staticOutput2 = [staticoutput,addoutput,');',newline];
                else
                    staticOutput2 = '';
                end

                createinputs = '';
                inputNames = '';
                if height(currentScreen.Workflow.Inputs) ~= 0
                    for i=1:length(currentScreen.Workflow.Inputs{:,1})
                        createinputs = [createinputs,['input_',char(i+64),' = createInput(drvObj,''',char(currentScreen.Workflow.Inputs.(1){i}),''',','''Datatype''',',''',char(currentScreen.Workflow.Inputs.(2){i}),''',','''Size''',',',char(currentScreen.Workflow.Inputs.(3){i}),');'],newline];
                    end
                end
                addinput = '';
                if height(currentScreen.Workflow.Inputs) ~= 0
                    staticinput ='addInput(drvObj,';
                    for i=1:length(currentScreen.Workflow.Inputs{:,1})
                        if i==length(currentScreen.Workflow.Inputs{:,1})
                            addinput = [addinput,['input_',char(i+64)]];
                        else
                            addinput = [addinput,['input_',char(i+64),',']];
                        end
                    end
                    staticInput2 = [staticinput,addinput,');',newline];
                else
                    staticInput2 = '';
                end

                createproperty = '';
                propertyNames = '';
                if height(currentScreen.Workflow.MaskParameters) ~= 0
                    for i=1:length(currentScreen.Workflow.MaskParameters{:,1})
                        if strcmp(char(currentScreen.Workflow.MaskParameters.(5){i}),'Non tunable')
                            booleanValue = 'false';
                        else
                            booleanValue = 'true';
                        end
                        createproperty = [createproperty,['property_',char(i+64),' = createProperty(drvObj,''',char(currentScreen.Workflow.MaskParameters.(1){i}),''',','''Datatype''',',''',char(currentScreen.Workflow.MaskParameters.(2){i}),''',','''Size''',',',char(currentScreen.Workflow.MaskParameters.(3){i}),',','''InitValue''',',',currentScreen.Workflow.MaskParameters.(4){i},',','''Tunable''',',',booleanValue,');'],newline];
                    end
                end
                addproperty = '';
                if height(currentScreen.Workflow.MaskParameters) ~= 0
                    staticproperty ='addProperty(drvObj,';
                    for i=1:length(currentScreen.Workflow.MaskParameters{:,1})
                        if i==length(currentScreen.Workflow.MaskParameters{:,1})
                            addproperty = [addproperty,['property_',char(i+64)]];
                        else
                            addproperty = [addproperty,['property_',char(i+64),',']];
                        end
                    end
                    staticProperty2 = [staticproperty,addproperty,');',newline];
                else
                    staticProperty2 = '';
                end


                minputarguments='';
                minputInputarguments='';
                if ((height(nextScreenObj.Workflow.Outputs) ~= 0) | (height(currentScreen.Workflow.Inputs) ~= 0 ))
                    for i=1:length(nextScreenObj.Workflow.Outputs{:,1})
                        if i==length(nextScreenObj.Workflow.Outputs{:,1})
                            minputarguments = [minputarguments,['output_',char(i+64)]];
                        else
                            minputarguments = [minputarguments,['output_',char(i+64),',']];
                        end
                    end
                    if (height(nextScreenObj.Workflow.Outputs) ~= 0 & (height(currentScreen.Workflow.Inputs) ~= 0 ))
                        minputarguments = [minputarguments,','];
                    end
                    for i=1:length(currentScreen.Workflow.Inputs{:,1})
                        if i==length(currentScreen.Workflow.Inputs{:,1})
                            minputInputarguments = [minputInputarguments,['input_',char(i+64)]];
                        else
                            minputInputarguments = [minputInputarguments,['input_',char(i+64),',']];
                        end
                    end
                    minputarguments=['mInputArgumentsToCStepFcn = {',minputarguments,minputInputarguments,'}'];
                end

                minputsetuparguments='';
                if (height(nextScreenObj.Workflow.MaskParameters) ~= 0)
                    for i=1:length(nextScreenObj.Workflow.MaskParameters{:,1})
                        if i==length(nextScreenObj.Workflow.MaskParameters{:,1})
                            minputsetuparguments = [minputsetuparguments,['property_',char(i+64)]];
                        else
                            minputsetuparguments = [minputsetuparguments,['property_',char(i+64),',']];
                        end
                    end
                    % if (size(nextScreenObj.Workflow.MaskParameters) ~= 0)
                    %     minputsetuparguments = [minputsetuparguments,','];
                    % end

                    minputsetuparguments=['mInputArgumentsToCSetupFcn = {',minputsetuparguments,'}'];
                end

                cinputarguments = '';
                cinputInputarguments = '';
                addInputStepArgs = '';
                if ((height(nextScreenObj.Workflow.Outputs) ~= 0) | (height(currentScreen.Workflow.Inputs) ~= 0 ))
                    for i=1:length(nextScreenObj.Workflow.Outputs{:,1})
                        if i==length(nextScreenObj.Workflow.Outputs{:,1})
                            if str2num(char(extractBefore(extractBetween(nextScreenObj.Workflow.Outputs.(3){i},'[',']'),',')))>1
                                if strcmp(char(nextScreenObj.Workflow.Outputs.(2){i}),'single')
                                    cinputarguments = [cinputarguments,['''','float',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint8')
                                    cinputarguments = [cinputarguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int8')
                                    cinputarguments = [cinputarguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint16')
                                    cinputarguments = [cinputarguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int16')
                                    cinputarguments = [cinputarguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint32')
                                    cinputarguments = [cinputarguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int32')
                                    cinputarguments = [cinputarguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint64')
                                    cinputarguments = [cinputarguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int64')
                                    cinputarguments = [cinputarguments,['''','int_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'boolean')
                                    cinputarguments = [cinputarguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                else
                                    cinputarguments = [cinputarguments,['''',char(nextScreenObj.Workflow.Outputs.(2){i}),' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),'''']];
                                end
                            else
                                if strcmp(char(nextScreenObj.Workflow.Outputs.(2){i}),'single')
                                    cinputarguments = [cinputarguments,['''','float',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint8')
                                    cinputarguments = [cinputarguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int8')
                                    cinputarguments = [cinputarguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint16')
                                    cinputarguments = [cinputarguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int16')
                                    cinputarguments = [cinputarguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint32')
                                    cinputarguments = [cinputarguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int32')
                                    cinputarguments = [cinputarguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint64')
                                    cinputarguments = [cinputarguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int64')
                                    cinputarguments = [cinputarguments,['''','int_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'boolean')
                                    cinputarguments = [cinputarguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                else
                                    cinputarguments = [cinputarguments,['''',char(nextScreenObj.Workflow.Outputs.(2){i}),' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),'''']];
                                end
                            end

                        else
                            if str2num(char(extractBefore(extractBetween(nextScreenObj.Workflow.Outputs.(3){i},'[',']'),',')))>1
                                if strcmp(char(nextScreenObj.Workflow.Outputs.(2){i}),'single')
                                    cinputarguments = [cinputarguments,['''','float',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+94),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint8')
                                    cinputarguments = [cinputarguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int8')
                                    cinputarguments = [cinputarguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint16')
                                    cinputarguments = [cinputarguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int16')
                                    cinputarguments = [cinputarguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint32')
                                    cinputarguments = [cinputarguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int32')
                                    cinputarguments = [cinputarguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint64')
                                    cinputarguments = [cinputarguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int64')
                                    cinputarguments = [cinputarguments,['''','int_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'boolean')
                                    cinputarguments = [cinputarguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                else
                                    cinputarguments = [cinputarguments,['''',char(nextScreenObj.Workflow.Outputs.(2){i}),' ** ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+96),''','''],['int size_',char(i+64),''',']];
                                end
                            else
                                if strcmp(char(nextScreenObj.Workflow.Outputs.(2){i}),'single')
                                    cinputarguments = [cinputarguments,['''','float',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),char(i+94),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint8')
                                    cinputarguments = [cinputarguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int8')
                                    cinputarguments = [cinputarguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint16')
                                    cinputarguments = [cinputarguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int16')
                                    cinputarguments = [cinputarguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint32')
                                    cinputarguments = [cinputarguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int32')
                                    cinputarguments = [cinputarguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'uint64')
                                    cinputarguments = [cinputarguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'int64')
                                    cinputarguments = [cinputarguments,['''','int_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Outputs.(2){i}),'boolean')
                                    cinputarguments = [cinputarguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                else
                                    cinputarguments = [cinputarguments,['''',char(nextScreenObj.Workflow.Outputs.(2){i}),' * ',char(nextScreenObj.Workflow.Outputs.(1){i}),''','''],['int size_',char(i+64),''',']];
                                end
                            end
                        end
                    end
                    if (height(nextScreenObj.Workflow.Outputs) ~= 0 & (height(currentScreen.Workflow.Inputs) ~= 0 ))
                        cinputarguments = [cinputarguments,','];
                    end

                    for i=1:length(currentScreen.Workflow.Inputs{:,1})
                        if i==length(currentScreen.Workflow.Inputs{:,1})
                            if str2num(char(extractBefore(extractBetween(currentScreen.Workflow.Inputs.(3){i},'[',']'),',')))>1
                                if strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'single')
                                    cinputInputarguments = [cinputInputarguments,['''','float',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint8')
                                    cinputInputarguments = [cinputInputarguments,['''','uint8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int8')
                                    cinputInputarguments = [cinputInputarguments,['''','int8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint16')
                                    cinputInputarguments = [cinputInputarguments,['''','uint16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int16')
                                    cinputInputarguments = [cinputInputarguments,['''','int16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint32')
                                    cinputInputarguments = [cinputInputarguments,['''','uint32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int32')
                                    cinputInputarguments = [cinputInputarguments,['''','int32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint64')
                                    cinputInputarguments = [cinputInputarguments,['''','uint_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int64')
                                    cinputInputarguments = [cinputInputarguments,['''','int_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'boolean')
                                    cinputInputarguments = [cinputInputarguments,['''','boolean_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                                else
                                    cinputInputarguments = [cinputInputarguments,['''',char(currentScreen.Workflow.Inputs.(2){i}),' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),'''']];
                                end
                            else
                                if strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'single')
                                    cinputInputarguments = [cinputInputarguments,['''','float',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint8')
                                    cinputInputarguments = [cinputInputarguments,['''','uint8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int8')
                                    cinputInputarguments = [cinputInputarguments,['''','int8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint16')
                                    cinputInputarguments = [cinputInputarguments,['''','uint16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int16')
                                    cinputInputarguments = [cinputInputarguments,['''','int16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint32')
                                    cinputInputarguments = [cinputInputarguments,['''','uint32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int32')
                                    cinputInputarguments = [cinputInputarguments,['''','int32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint64')
                                    cinputInputarguments = [cinputInputarguments,['''','uint_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int64')
                                    cinputInputarguments = [cinputInputarguments,['''','int_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'boolean')
                                    cinputInputarguments = [cinputInputarguments,['''','boolean_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                                else
                                    cinputInputarguments = [cinputInputarguments,['''',char(currentScreen.Workflow.Inputs.(2){i}),' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),'''']];
                                end
                            end
                        else
                            if str2num(char(extractBefore(extractBetween(currentScreen.Workflow.Inputs.(3){i},'[',']'),',')))>1                          
                                if strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'single')
                                    cinputInputarguments = [cinputInputarguments,['''','float',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint8')
                                    cinputInputarguments = [cinputInputarguments,['''','uint8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int8')
                                    cinputInputarguments = [cinputInputarguments,['''','int8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint16')
                                    cinputInputarguments = [cinputInputarguments,['''','uint16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int16')
                                    cinputInputarguments = [cinputInputarguments,['''','int16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint32')
                                    cinputInputarguments = [cinputInputarguments,['''','uint32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int32')
                                    cinputInputarguments = [cinputInputarguments,['''','int32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint64')
                                    cinputInputarguments = [cinputInputarguments,['''','uint_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int64')
                                    cinputInputarguments = [cinputInputarguments,['''','int_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'boolean')
                                    cinputInputarguments = [cinputInputarguments,['''','boolean_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                                else
                                    cinputInputarguments = [cinputInputarguments,['''',char(currentScreen.Workflow.Inputs.(2){i}),' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),char(i+64),''','''],['int size_',char(i+96),''',']];
                                end
                            else
                                if strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'single')
                                    cinputInputarguments = [cinputInputarguments,['''','float',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint8')
                                    cinputInputarguments = [cinputInputarguments,['''','uint8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int8')
                                    cinputInputarguments = [cinputInputarguments,['''','int8_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint16')
                                    cinputInputarguments = [cinputInputarguments,['''','uint16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int16')
                                    cinputInputarguments = [cinputInputarguments,['''','int16_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint32')
                                    cinputInputarguments = [cinputInputarguments,['''','uint32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int32')
                                    cinputInputarguments = [cinputInputarguments,['''','int32_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'uint64')
                                    cinputInputarguments = [cinputInputarguments,['''','uint_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'int64')
                                    cinputInputarguments = [cinputInputarguments,['''','int_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                                elseif strcmp(char(currentScreen.Workflow.Inputs.(2){i}),'boolean')
                                    cinputInputarguments = [cinputInputarguments,['''','boolean_T',' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                                else
                                    cinputInputarguments = [cinputInputarguments,['''',char(currentScreen.Workflow.Inputs.(2){i}),' ',char(currentScreen.Workflow.Inputs.(1){i}),''','''],['int size_',char(i+96),''',']];
                                end
                            end
                        end
                    end
                    if (height(nextScreenObj.Workflow.Outputs) ~= 0 & (height(currentScreen.Workflow.Inputs) ~= 0 ))
                        cinputarguments = ['cInputArgumentstoStepFcn = {',cinputarguments,cinputInputarguments,'};'];
                    elseif height(nextScreenObj.Workflow.Outputs) == 0
                        cinputarguments = ['cInputArgumentstoStepFcn = {',cinputInputarguments,'};'];
                    else
                        cinputarguments = ['cInputArgumentstoStepFcn = {',cinputarguments,'};'];
                    end
                    addInputStepArgs = 'addInputStepArguments(drvObj,cInputArgumentstoStepFcn,mInputArgumentsToCStepFcn);';
                end

                cinputsetuparguments = '';
                addInputSetupArgs = '';
                if (height(nextScreenObj.Workflow.MaskParameters) ~= 0)
                    for i=1:length(nextScreenObj.Workflow.MaskParameters{:,1})
                        if i==length(nextScreenObj.Workflow.MaskParameters{:,1})
                            if str2num(char(extractBefore(extractBetween(currentScreen.Workflow.MaskParameters.(3){i},'[',']'),',')))>1
                                if strcmp(char(nextScreenObj.Workflow.MaskParameters.(2){i}),'single')
                                    cinputsetuparguments = [cinputsetuparguments,['''','float',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'boolean')
                                    cinputsetuparguments = [cinputsetuparguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                else
                                    cinputsetuparguments = [cinputsetuparguments,['''',char(nextScreenObj.Workflow.MaskParameters.(2){i}),' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64)]];
                                end
                            else
                                if strcmp(char(nextScreenObj.Workflow.MaskParameters.(2){i}),'single')
                                    cinputsetuparguments = [cinputsetuparguments,['''','float',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'boolean')
                                    cinputsetuparguments = [cinputsetuparguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                else
                                    cinputsetuparguments = [cinputsetuparguments,['''',char(nextScreenObj.Workflow.MaskParameters.(2){i}),' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64)]];
                                end
                            end
                        else
                            if str2num(char(extractBefore(extractBetween(currentScreen.Workflow.MaskParameters.(3){i},'[',']'),',')))>1
                                if strcmp(char(nextScreenObj.Workflow.MaskParameters.(2){i}),'single')
                                    cinputsetuparguments = [cinputsetuparguments,['''','float',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'boolean')
                                    cinputsetuparguments = [cinputsetuparguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                else
                                    cinputsetuparguments = [cinputsetuparguments,['''',char(nextScreenObj.Workflow.MaskParameters.(2){i}),' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+96),char(i+94),char(i+64),''','''],['int size_',char(i+64),''',']];
                                end
                            else
                                if strcmp(char(nextScreenObj.Workflow.MaskParameters.(2){i}),'single')
                                    cinputsetuparguments = [cinputsetuparguments,['''','float',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int8')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int8_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int16')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int16_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int32')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int32_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'uint64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','uint_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'int64')
                                    cinputsetuparguments = [cinputsetuparguments,['''','int_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                elseif strcmp(char(currentScreen.Workflow.MaskParameters.(2){i}),'boolean')
                                    cinputsetuparguments = [cinputsetuparguments,['''','boolean_T',' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                else
                                    cinputsetuparguments = [cinputsetuparguments,['''',char(nextScreenObj.Workflow.MaskParameters.(2){i}),' * ',char(nextScreenObj.Workflow.MaskParameters.(1){i}),''','''],['int size_',char(i+64),''',']];
                                end
                            end
                        end
                    end
                    % if (size(nextScreenObj.Workflow.MaskParameters) ~= 0)
                    %     cinputsetuparguments = [cinputsetuparguments,','];
                    % end
                    cinputsetuparguments = ['cInputArgumentstoSetupFcn = {',cinputsetuparguments,'''};'];
                    addInputSetupArgs = 'addInputSetupArguments(drvObj,cInputArgumentstoSetupFcn,mInputArgumentsToCSetupFcn);';
                end

                nextScreenObj.Workflow.processTemplateFile('createDeviceDriverScript.m','ExecutionScript.m',...
                    {'##Block_Name##',['''',nextScreenObj.Workflow.BlockName,''''],...
                    '##Block_Mask_Description##',['''',nextScreenObj.Workflow.BlockDescription,''''], ...
                    '##Block_Type##','''Source''',....
                    '##Headerfile_Path##',headerfilesPath,...
                    '##Sourcefile_Path##',sourcefilesPath,...
                    '##Sourcefiles##',sourcefiles,...
                    '##Output_Declaration##',createoutputs,...
                    '##Addoutput##',staticOutput2,...
                    '##Input_Declaration##',createinputs,...
                    '##Addinput##',staticInput2,...
                    '##Property_Declaration##',createproperty,...
                    '##Addproperty##',staticProperty2,...
                    '##MinputArguments##',minputarguments,...
                    '##CinputArguments##',cinputarguments,...
                    '##MinputSetupArguments##',minputsetuparguments,...
                    '##CinputSetupArguments##',cinputsetuparguments,...
                    '##addStepArguments##',addInputStepArgs,...
                    '##addSetupArguments##',addInputSetupArgs,...
                    '##WorkingDirectory##',workingdirectory,
                    });
                run('ExecutionScript.m');
                delete(fullfile(currentScreen.Workflow.WorkingDir,[currentScreen.Workflow.BlockName,'789.jpg']));
                delete 'ExecutionScript.m';
            end
            try
                % Display Next Screen
                nextScreenObj.show();
                currentScreen.hide();

                % If hardware setup is running inside hardware manager,
                % then resize
                if ~isempty(currentScreen.Workflow.HardwareManagerAppResizeFcn)
                    currentScreen.Workflow.HardwareManagerAppResizeFcn();
                end
            catch ex
                currentScreen.logMessage(['Error navigating to the next screen -- ' ...
                    currentScreen.Workflow.Name ':' class(nextScreenObj) newline 'Details:' ex.message]);
                error(message('hwsetup:template:NavigationFailureNext', ex.message));
            end
        end

        function backButtonCallback(~, ~, currentScreen)
            % backButtonCallback - Callback Handler for the Back Button
            % widget.The activities performed during the "Back" Button
            % Callback
            % 1. Get the ID for the Previous Screen
            % 2. Create the screen object for the previous screen
            % 3. Save the context of the current screen
            % 4. Delete the current screen
            % 5. Display the previous screen

            backScreenObj = currentScreen.getPreviousScreenObj();
            try
                currentScreen.hide();
                backScreenObj.show();
                % If hardware setup is running inside hardware manager,
                % then resize
                if ~isempty(currentScreen.Workflow.HardwareManagerAppResizeFcn)
                    currentScreen.Workflow.HardwareManagerAppResizeFcn();
                end
            catch ex
                currentScreen.logMessage(['Error navigating to the previous screen -- ' ...
                    currentScreen.Workflow.Name ':' class(backScreenObj) newline 'Details:' ex.message]);
                error(message('hwsetup:template:NavigationFailureBack', ex.message));
            end
        end

        function finish(src, ~, screen)
            % exit by default, unless confirmation dialog has been enabled
            choice = message('hwsetup:template:CancelButtonDlgExitText').getString();

            if isequal(screen.Workflow.CancelConfirmationDlg, 'on')
                % finish() will be called on Finish or Cancel button clicks
                if isprop(src, 'Text')
                    if isequal(src.Text, message('hwsetup:template:CancelButtonText').getString())
                        choice = questdlg(message('hwsetup:template:CancelButtonDlgText').getString(), ... question to be asked
                            message('hwsetup:template:CancelButtonDlgTitle').getString(),... dialog title
                            message('hwsetup:template:CancelButtonDlgExitText').getString(),... option 1
                            message('hwsetup:template:CancelButtonDlgBackText').getString(),... option 2
                            message('hwsetup:template:CancelButtonDlgBackText').getString()... default selection
                            );
                    end
                end
            end

            if isequal(choice, message('hwsetup:template:CancelButtonDlgExitText').getString())
                % if hardware setup is running inside hardware manager
                if ~isempty(screen.Workflow.HardwareManagerCloseAppFcn)
                    screen.Workflow.HardwareManagerCloseAppFcn();
                else
                    screen.Workflow.delete();
                end
            end
        end

        function setWidgetTags(screen)
            % SETWIDGETTAGS - Sets the Tag property for the all properties
            % of the screen object that are of type

            validateattributes(screen, {'matlab.hwmgr.internal.hwsetup.TemplateBase'}...
                ,{'nonempty'});

            % Get all the widgets
            widgetNames = matlab.hwmgr.internal.hwsetup.TemplateBase.getWidgetPropertyNames(screen);
            for i = 1:numel(widgetNames)
                tag = screen.(widgetNames{i}).Tag;
                if isempty(tag) || contains(tag, 'ContentPanel')
                    screen.(widgetNames{i}).Tag = ...
                        matlab.hwmgr.internal.hwsetup.getNameTag(...
                        [class(screen) '_' widgetNames{i}]);
                end
            end
            screen.Workflow.Window.Tag =  matlab.hwmgr.internal.hwsetup.getNameTag(...
                [class(screen) '_Window' ]);
        end

        function widgetNames = getWidgetPropertyNames(screen)
            % GETWIDGETPROPERTYNAMES - Returns the names of properties for
            % a given object that are of type
            % matlab.hwmgr.internal.hwsetup.Widget or
            % matlab.hwmgr.internal.hwsetup.DerivedWidget
            % The widget properties must be valid i.e. not a handle to a
            % deleted object

            validateattributes(screen, {'matlab.hwmgr.internal.hwsetup.TemplateBase'}...
                ,{'nonempty'});
            % Find all properties
            mco = metaclass(screen);
            propNames = {mco.PropertyList.Name};
            % Find superclasses for the properties
            propSuperClasses = {};
            filterIdx = zeros(1, numel(propNames));
            for i=1:numel(propNames)
                try
                    % If widget-property has a method isvalid, check if
                    % widget is valid. Widget properties might be deleted
                    % by screen methods.
                    if ismethod(screen.(propNames{i}), 'isvalid')
                        filterIdx(i) = ~screen.(propNames{i}).isvalid;
                    end
                    propSuperClasses{i} = superclasses(screen.(propNames{i})); %#ok<*AGROW>
                catch
                    % Error for properties that do not have access to
                    % TemplateBase class
                    propSuperClasses{i} = {};
                end
            end
            propNames = propNames(~filterIdx);
            propSuperClasses = propSuperClasses(~filterIdx);
            % If the superclass list contains
            % matlab.hwmgr.internal.hwsetup.Widget, property is a widget
            isPropWidgetIdx = cellfun(@(x)any(ismember(x,...
                {'matlab.hwmgr.internal.hwsetup.Widget', 'matlab.hwmgr.internal.hwsetup.DerivedWidget'}))...
                , propSuperClasses);
            widgetNames = propNames(isPropWidgetIdx);

        end

        function widgetList = getAllTemplateWidgetLayoutDetails()
            % GETALLTEMPLATEWIDGETLAYOUTDETAILS - Get the type, name and
            % parent for the widget that are rendered in the Base Template

            panelType = 'matlab.hwmgr.internal.hwsetup.Panel';
            gridType = 'matlab.hwmgr.internal.hwsetup.Grid';
            buttonType = 'matlab.hwmgr.internal.hwsetup.Button';
            labelType = 'matlab.hwmgr.internal.hwsetup.Label';
            htmlTextType = 'matlab.hwmgr.internal.hwsetup.HTMLText';
            helpTextType = 'matlab.hwmgr.internal.hwsetup.HelpText';

            w = 1;

            widgetList(w).Name = 'ParentGrid';
            widgetList(w).Type = gridType;
            widgetList(w).Parent = 'DEFAULT';
            w = w + 1;

            widgetList(w).Name = 'Banner';
            widgetList(w).Type = gridType;
            widgetList(w).Parent = 'ParentGrid';
            w = w + 1;

            widgetList(w).Name = 'WorkflowSteps';
            widgetList(w).Type = htmlTextType;
            widgetList(w).Parent = 'Banner';
            w = w + 1;

            widgetList(w).Name = 'Title';
            widgetList(w).Type = labelType;
            widgetList(w).Parent = 'Banner';
            w = w + 1;

            widgetList(w).Name = 'ContentPanel';
            widgetList(w).Type = panelType;
            widgetList(w).Parent = 'ParentGrid';
            w = w + 1;

            widgetList(w).Name = 'ContentGrid';
            widgetList(w).Type = gridType;
            widgetList(w).Parent = 'ContentPanel';
            w = w + 1;

            widgetList(w).Name = 'HelpText';
            widgetList(w).Type = helpTextType;
            widgetList(w).Parent = 'ParentGrid';
            w = w +1 ;

            widgetList(w).Name = 'NavigationGrid';
            widgetList(w).Type = gridType;
            widgetList(w).Parent = 'ParentGrid';
            w = w + 1;

            widgetList(w).Name = 'BackButton';
            widgetList(w).Type = buttonType;
            widgetList(w).Parent = 'NavigationGrid';
            w = w + 1;

            widgetList(w).Name = 'CancelButton';
            widgetList(w).Type = buttonType;
            widgetList(w).Parent = 'NavigationGrid';
            w = w + 1;

            widgetList(w).Name = 'NextButton';
            widgetList(w).Type = buttonType;
            widgetList(w).Parent = 'NavigationGrid';
        end
    end

    % Methods to enable testing of screens and templates
    methods(Access = {?hwsetuptest.util.TemplateBaseTester,...
            ?hwsetup.testtool.TesterBase})
        function out = getWidgetTypeAndTag(obj)
            allWidgetNames = matlab.hwmgr.internal.hwsetup.TemplateBase.getWidgetPropertyNames(obj);
            out = containers.Map();
            for i = 1:numel(allWidgetNames)
                widgetTag = obj.(allWidgetNames{i}).Tag;
                if ~isempty(widgetTag)
                    [~, ~, widgetType] = fileparts(class(obj.(allWidgetNames{i})));
                    widgetType = regexprep(widgetType, '\.','');
                    out(widgetTag) = widgetType;
                end
            end
            out(obj.Workflow.Window.Tag) = 'Window';
        end
    end
end