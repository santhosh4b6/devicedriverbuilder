% Copyright 2023 The MathWorks, Inc.
function manageTableRows(hTable, action)

switch(action)

    case 'remove'
        tableData = hTable.Data;
        tableData(hTable.Selection, :) = [];
        hTable.Data = tableData;
    case 'moveup'
        selectedRows = hTable.Selection;
        if any(selectedRows == 1), return; end
        for i = 1:numel(selectedRows)
            currentRow = selectedRows(i);
            % swap currentRow with previousRow
            temp = hTable.Data(currentRow-1, :);
            hTable.Data(currentRow - 1, :) = hTable.Data(currentRow, :);
            hTable.Data(currentRow, :) = temp;
        end
        hTable.Selection = hTable.Selection - 1;
    case 'movedown'
        selectedRows = hTable.Selection;
        if any(selectedRows == height(hTable.Data)), return; end
        for i = 1:numel(selectedRows)
            currentRow = selectedRows(i);
            % swap currentRow with nextRow
            temp = hTable.Data(currentRow+1, :);
            hTable.Data(currentRow+1, :) = hTable.Data(currentRow, :);
            hTable.Data(currentRow, :) = temp;
        end
        hTable.Selection = hTable.Selection + 1;
end
end