classdef DeviceDriverBuilder < matlab.hwmgr.internal.hwsetup.Workflow
    % This is the main workflow file for Device Driver Builder app

    % Copyright 2023 The MathWorks, Inc.
    properties
        % Inherited
        Name
        FirstScreenID
        FinDir
        FolderPath
        folders
        SrcFiles
        Outputs
        Inputs
        MaskParameters
        BlockName
        BlockLabel
        BlockDescription
        WorkingDir
    end
    
    methods
         % Class Constructor
        function obj = DeviceDriverBuilder(varargin)
            obj@matlab.hwmgr.internal.hwsetup.Workflow(varargin{:})
            obj.Name = 'Device Driver Builder';
            obj.FirstScreenID = 'IntroductionScreen';
            obj.Window.Resize = 'on';
            obj.Window.Position = [940 480 1000 570];
            obj.Window.Title = 'Device Driver Builder';
        end

        function processTemplateFile(~,inputFile,outputFile,args)
            %PROCESSTEMPLATEFILE
            %
            % processTemplateFile(inputFile,outputFile,varargin)
            %
            % Example:
            %
            % processTemplateFile('boardClassTemplate.m','chippro.m',...
            %    {'##BOARDNAME##','chippro','boardClassTemplate','chippro'})
            
            % read whole file data into an array
            if nargin > 0
                inputFile = convertStringsToChars(inputFile);
            end
            
            if nargin > 1
                outputFile = convertStringsToChars(outputFile);
            end
            validateattributes(args,{'cell'},{'nonempty'},'processTemplateFile','args');
            fid = fopen(inputFile,'r');
            txt = fread(fid,'*char')';
            fclose(fid);
            
            % Replace the Search string with Replace String
            for i = 1:2:numel(args)
                txt = strrep(txt,args{i},args{i+1});
            end
            
            % Write it back into the file
            fid  = fopen(outputFile,'w');
            fwrite(fid,txt,'char');
            fclose(fid);
        end
    end
end