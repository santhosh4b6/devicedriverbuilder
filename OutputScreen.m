classdef OutputScreen < BreadCrumb
    %OutputScreen is one screen that is meant
    %to be included in a package of screens that make up a Device Driver Builder app. It presents user a way to define outputs of system object.

    % Copyright 2023 The MathWorks, Inc.

    properties(Access = public)
        ResultLabel
        AddBtn
        RemoveButton
        Table
        TableVal
        MoveUpButton
        MoveDownButton
        BlockImageOutput
    end

    properties(Access = private)
        dataTypeStr
    end

    properties(Access = private, Constant = true)
        FontSize = 13
        BreadCrumbStep = 5
    end

    methods(Access = 'public')
        function obj = OutputScreen(workflow)
            validateattributes(workflow, {'matlab.hwmgr.internal.hwsetup.Workflow'}, {});
            obj@BreadCrumb(workflow,matlab.hwmgr.internal.hwsetup.TemplateLayout.GRID);
            obj.setCurrentStep(obj.BreadCrumbStep);
            obj.ContentGrid.RowHeight = {'1x','fit', '1x',24};
            obj.ContentGrid.ColumnWidth = {'1x', '1x', '1x','1x'};
            obj.BlockImageOutput = matlab.hwmgr.internal.hwsetup.Image.getInstance(obj.ContentGrid);
            obj.BlockImageOutput.Row = 1;
            obj.BlockImageOutput.Column = [2,3];
            obj.ResultLabel = matlab.hwmgr.internal.hwsetup.Label.getInstance(obj.ContentGrid);
            obj.ResultLabel.Text = 'Add and define block outputs below:';
            obj.ResultLabel.Row = 2;
            obj.ResultLabel.Column = [1, 3];
            obj.ResultLabel.FontWeight = 'bold';            
            obj.Table = matlab.hwmgr.internal.hwsetup.Table.getInstance(obj.ContentGrid);
            obj.Table.Tag = 'Example_Table';
            obj.Table.Row = 3;
            obj.Table.Column = [1, 4];
            obj.Table.ColumnName = {'Name', 'Data type', 'Dimension'};
            obj.AddBtn = matlab.hwmgr.internal.hwsetup.Button.getInstance(obj.ContentGrid);
            obj.RemoveButton = matlab.hwmgr.internal.hwsetup.Button.getInstance(obj.ContentGrid);
            obj.AddBtn.Text = "Add";
            % AddBtn.setIconID('openFolder');
            obj.AddBtn.ButtonPushedFcn =  @obj.addRow;
            obj.AddBtn.setIconID('add', 16, 16);
            obj.RemoveButton.Text = "Remove";
            % obj.RemoveButton.setIconID('openFolder');
            obj.RemoveButton.ButtonPushedFcn = @obj.removeRow;
            obj.RemoveButton.Row = 4;
            obj.RemoveButton.Column = 2;
            obj.RemoveButton.setIconID('errorCross', 16, 16);
            obj.MoveUpButton = matlab.hwmgr.internal.hwsetup.Button.getInstance(obj.ContentGrid);
            obj.MoveUpButton.Text = "Move up";
            obj.MoveUpButton.ButtonPushedFcn = @obj.moveRowUp;
            obj.MoveUpButton.setIconID('arrowNavigationNorth', 16, 16);
            obj.MoveDownButton = matlab.hwmgr.internal.hwsetup.Button.getInstance(obj.ContentGrid);
            obj.MoveDownButton.Text = "Move down";
            obj.MoveDownButton.ButtonPushedFcn = @obj.moveRowDown;
            obj.MoveDownButton.setIconID('arrowNavigationSouth', 16, 16);
            obj.AddBtn.Row = 4;
            obj.AddBtn.Column = 1;

            obj.MoveUpButton.Row = 4;
            obj.MoveUpButton.Column = 3;
            obj.MoveDownButton.Row = 4;
            obj.MoveDownButton.Column = 4;

            obj.Table.ColumnEditable = logical([1 1 1]);
            obj.TableVal = obj.Table.getPeer();
            obj.TableVal.SelectionType = 'row';
            obj.TableVal.SelectionChangedFcn = @obj.selectionChangedFcn;
            obj.TableVal.CellEditCallback = @obj.validateTableData;

            slDataTypes = {...
                'int8',...
                'uint8',...
                'int16',...
                'uint16',...
                'int32',...
                'uint32',...
                'int64',...
                'uint64',...
                'single',...
                'double',...
                'boolean',...
                };
            obj.dataTypeStr = @(x)categorical({x}, unique([slDataTypes {x}], 'stable'));
            obj.Table.Data = table({"Output1"},{obj.dataTypeStr('int8')}, {"[1,1]"});
            updateScreen(obj);
            obj.HelpText.AboutSelection = '';
            obj.HelpText.WhatToConsider = 'Please enter the Block outputs details in this screen. To decide about required Block outputs open the required .ino file from the downloaded files and search for outputs in the loop function. For example for adxl345 accelerometer sensor acceleration is one output which will be present in its adafruit github files.';
        end

        function id = getPreviousScreenID(~)
            id = 'BlockParametersScreen';
        end

        function  id = getNextScreenID(obj)
            id = 'InputScreen';
        end

        function reinit(obj)
            updateScreen(obj);
        end

        function addRow(obj, ~, ~)
            obj.TableVal.Data =  [obj.TableVal.Data; table({"Output"},{obj.dataTypeStr('int8')}, {"[1,1]"})];
        end

        function removeRow(obj,~,~)
            manageTableRows(obj.Table, 'remove');
        end

        function moveRowUp(obj, ~, ~)
            manageTableRows(obj.Table, 'moveup');
        end

        function moveRowDown(obj, ~, ~)
            manageTableRows(obj.Table, 'movedown');
        end

        function show(obj)
            %Overwrite show method to hide Back button on Mac and Linux
            %since it is the entry screen for postinstall setup
            show@matlab.hwmgr.internal.hwsetup.TemplateBase(obj)
            if isunix
                obj.BackButton.Visible = 'off';
            end
        end

        function selectionChangedFcn(obj, src, ~)

        end

        function validateTableData(obj, ~, event)
            indices = event.Indices;
            newData = event.NewData;
            if ~isempty(obj.Workflow.Outputs)
                obj.Workflow.Outputs.(indices(2)){indices(1)} = newData;
            end
            try
                if indices(2) == 4
                    param = 'Dimension';
                    numVal = newData;
                    % UI only allows scalar values
                    % If user enter any other values (like [1 2]), newData
                    % will be NaN
                    if isscalar(numVal) && mod(numVal, 1) == 0 % scalar and integer
                        try
                            validateattributes(numVal, {'numeric'}, {'nonempty', 'positive', 'nonzero'}, '', param);
                            return;
                        catch ME
                            obj.DataInterfaces.Data.Dimension{indices(1)} = event.PreviousData;
                            rethrow(ME);
                        end
                    end
                    error(message('ERRORHANDLER:utils:InvalidInputParameter', 'Block interfaces', event.EditData, param));
                elseif indices(2) == 3
                    % Data type
                    unSupportedBuiltInDT = {'double','single'};
                    try
                        DataType = char(newData);
                        DataType = evalin('base', DataType);
                    catch
                        % built-in type
                    end

                    if isa(DataType, 'Simulink.AliasType')
                        try
                            DataType = DataType.BaseType;
                            DataType = evalin('base', DataType);
                        catch
                            % built-in type
                        end
                    end

                    switch class(DataType)
                        case'Simulink.NumericType'
                            % To do: Validate for word length
                        case 'double'
                        case 'char'
                            % To do: Validate for word length
                            if any(strcmpi(DataType, unSupportedBuiltInDT)) || isempty(regexp(DataType, '^[A-Za-z]\w+$', 'ONCE'))
                                error(message('soc:msgs:InvalidDataType', DataType).getString);
                            end
                        otherwise
                            error(message('soc:msgs:InvalidDataType', DataType).getString);
                    end
                end
            catch ME
                obj.DataInterfaces.Data.(['Var' num2str(indices(2))]){indices(1)} = event.PreviousData;
                obj.Workflow.throwError(ME.message);
            end
        end
    end

    methods(Access = 'private')
        function updateScreen(obj)

        end

        function updateSetupFlag(obj, src, ~)

        end
    end

end
