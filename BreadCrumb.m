classdef BreadCrumb < matlab.hwmgr.internal.hwsetup.TemplateBase
    % This setup screen template has widget for breadcrumbs

    % Copyright 2023 The MathWorks, Inc.

    properties

        % Current Bread crumb step
        CurrentStep

        % All Bread crumb steps
        Steps

    end

    methods
        function this = BreadCrumb(varargin)
            % Constructor

            % Initialize template
            this@matlab.hwmgr.internal.hwsetup.TemplateBase(varargin{:});

            this.Title.Position = [20 1 770 25];
            this.Steps = matlab.hwmgr.internal.hwsetup.HTMLText.getInstance(this.Banner);
            this.Steps.Position = [20 96 700 28];
            this.setCurrentStep(1); %highlight step 1 by default
            % this.Title.Row = 1;
            %  this.Title.Column = [1,  7];
            if isprop(this.Steps, 'BackgroundColor')
                this.Steps.BackgroundColor = [0 0.3294 0.5804];
            end
            % this.Steps.FontWeight = 'bold';
        end

        function setCurrentStep(this, currentStep)
            % Set breadcrumb properties

            steps = this.getStepsToDisplay();
            steps{currentStep} = ['<font color="white"><b>' steps{currentStep} '</b></font>'];
            this.Steps.Text = sprintf('<body bgcolor = "#005494"><font color="#C0C0C0">%s</font></body>', strjoin(steps, '&nbsp;&nbsp;>&nbsp;&nbsp;'));
            this.CurrentStep = currentStep;

        end

    end

    methods (Access = private)
        function steps = getStepsToDisplay(~)
            % Bread crumb steps
            steps = {
                'Welcome','Source files location', 'Source files'...
                'Block parameters', 'Outputs',...
                'Inputs','Block image','Generate'
                };
        end
    end
end
