classdef IntroductionScreen < BreadCrumb
    %IntroductionScreen is one screen that is meant
    %to be included in a package of screens that make up a Device Driver Builder app. There
    %is a Workflow object that is passed from screen to screen.
    %
    % The IntroductionScreen is used to give the overview of the app to the
    % user.

    % Copyright 2023 The MathWorks, Inc.

    properties(Access = public)
        % HelpText for Doc link
        DocLink
        % ResultLabel  
        AppNameText
        WelcomeText
        WelcomeAppDescriptionHeadingText
    end
    
    properties(Access = private, Constant = true)
        FontSize = 13
        BreadCrumbStep = 1
    end

    methods(Access = 'public')
        function obj = IntroductionScreen(workflow)
            validateattributes(workflow, {'matlab.hwmgr.internal.hwsetup.Workflow'}, {});
            obj@BreadCrumb(workflow,matlab.hwmgr.internal.hwsetup.TemplateLayout.GRID);
            obj.setCurrentStep(obj.BreadCrumbStep);
            obj.ContentGrid.RowHeight = {'fit', 'fit', 'fit', 'fit', 'fit','fit','fit','fit','fit','fit','fit','fit','fit','fit','fit','fit','fit'};
            obj.ContentGrid.ColumnWidth = {'1x', '1x', '1x', '1x','1x'};
            obj.BackButton.Enable = 'off';
            obj.BackButton.Visible = 'off';
            % App name related settings
            obj.AppNameText = matlab.hwmgr.internal.hwsetup.Label.getInstance(obj.ContentGrid);
            obj.AppNameText.Text = "Device Driver Builder";
            obj.AppNameText.Row = 2;
            obj.AppNameText.Column = [3 4];  
            obj.AppNameText.FontWeight = 'bold';
            obj.AppNameText.FontSize = 15;
            % Welcome text related settings
            obj.WelcomeText = matlab.hwmgr.internal.hwsetup.HTMLText.getInstance(obj.ContentGrid);
            obj.WelcomeText.Text = "The Device Driver Builder allows you to create a system object for custom or third party(3p) source files (c/c++). The Device Driver Builder also allows you to define inputs, outputs and parameters of the system object";
            obj.WelcomeText.Row = [4 5];
            obj.WelcomeText.Column = [1 5];   

            % Welcome AppDescription Heading Text related settings
            obj.WelcomeAppDescriptionHeadingText = matlab.hwmgr.internal.hwsetup.HTMLText.getInstance(obj.ContentGrid);
            obj.WelcomeAppDescriptionHeadingText.Text = "Generated system object can be used to create a Simulink Block using MATLAB system block in the Simulink model.";
            obj.WelcomeAppDescriptionHeadingText.Row = 11;
            obj.WelcomeAppDescriptionHeadingText.Column = [1 5];
            updateScreen(obj);
            % Hidding the help text area for the welcom text screen 
            obj.HelpText.AboutSelection = '';
            obj.HelpText.WhatToConsider = '';
            obj.HelpText.Visible = 'off';          
        end

        function id = getPreviousScreenID(~)
            id = 'arduinoio.setup.internal.DriverInstallScreen';
        end

        function  id = getNextScreenID(obj)         
                id = 'SelectWorkingDirectoryScreen';            
        end

        function reinit(obj)
            updateScreen(obj);
        end
        
        function show(obj)
            %Overwrite show method to hide Back button on Mac and Linux
            %since it is the entry screen for postinstall setup
            show@matlab.hwmgr.internal.hwsetup.TemplateBase(obj)
            if isunix
                obj.BackButton.Visible = 'off';
            end
        end
    end

    methods(Access = 'private')
        function updateScreen(obj)
           
        end
        
        function updateSetupFlag(obj, src, ~)
            
        end
    end

end
