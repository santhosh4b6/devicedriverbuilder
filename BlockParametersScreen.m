classdef BlockParametersScreen < BreadCrumb
    %BlockParametersScreen is one screen that is meant
    %to be included in a package of screens that make up a Device Driver Builder app. It presents user a way to define block name and parametes of system object.

    % Copyright 2023 The MathWorks, Inc.

    properties(Access = public)
        ResultLabel
        AddBtn
        RemoveButton
        EditBlockName
        LabelMaskName
        EditBlockLabel
        LabelBlockLabel
        EditBlockDescription
        LabelBlockDescription
        TableVal
        Table
        MoveUpButton
        MoveDownButton
    end

    properties(Access = private, Constant = true)
        FontSize = 13
        BreadCrumbStep = 4
    end

    properties(Access = private)
        dataTypeStr
        TypeStr
    end

    methods(Access = 'public')
        function obj = BlockParametersScreen(workflow)
            validateattributes(workflow, {'matlab.hwmgr.internal.hwsetup.Workflow'}, {});
            obj@BreadCrumb(workflow,matlab.hwmgr.internal.hwsetup.TemplateLayout.GRID);
            obj.setCurrentStep(obj.BreadCrumbStep);
            obj.ContentGrid.RowHeight = {'fit', 24, 24, 'fit','1x',24};
            obj.ContentGrid.ColumnWidth = {'1x','1x','1x','1x','1x'};
            obj.ResultLabel = matlab.hwmgr.internal.hwsetup.Label.getInstance(obj.ContentGrid);
            obj.ResultLabel.Text = 'Add and define mask parameters below:';
            obj.ResultLabel.Row = 1;
            obj.ResultLabel.Column = [1, 3];
            obj.ResultLabel.FontWeight = 'bold';
            obj.AddBtn = matlab.hwmgr.internal.hwsetup.Button.getInstance(obj.ContentGrid);
            obj.RemoveButton = matlab.hwmgr.internal.hwsetup.Button.getInstance(obj.ContentGrid);
            obj.AddBtn.Text = "Add";
            obj.AddBtn.ButtonPushedFcn = @obj.addRow;
            obj.AddBtn.setIconID('add', 16, 16);
            obj.RemoveButton.Text = "Remove";
            obj.RemoveButton.ButtonPushedFcn = @obj.removeRow;
            obj.RemoveButton.setIconID('errorCross', 16, 16);

            obj.MoveUpButton = matlab.hwmgr.internal.hwsetup.Button.getInstance(obj.ContentGrid);
            obj.MoveUpButton.Text = "Move up";
            obj.MoveUpButton.ButtonPushedFcn = @obj.moveRowUp;
            obj.MoveUpButton.setIconID('arrowNavigationNorth', 16, 16);

            obj.MoveDownButton = matlab.hwmgr.internal.hwsetup.Button.getInstance(obj.ContentGrid);
            obj.MoveDownButton.Text = "Move down";
            obj.MoveDownButton.ButtonPushedFcn = @obj.moveRowDown;
            obj.MoveDownButton.setIconID('arrowNavigationSouth', 16, 16);

            obj.EditBlockName = matlab.hwmgr.internal.hwsetup.EditText.getInstance(obj.ContentGrid);
            obj.EditBlockName.TextAlignment = 'left';
            obj.EditBlockName.Row = 2;
            obj.EditBlockName.Column = [2, 4];
            obj.EditBlockName.ValueChangedFcn = @obj.captureBlockName;

            obj.LabelMaskName = matlab.hwmgr.internal.hwsetup.Label.getInstance(obj.ContentGrid);
            obj.LabelMaskName.Text = 'Block name:';
            obj.LabelMaskName.Row = 2;
            obj.LabelMaskName.Column = 1;

            obj.EditBlockLabel = matlab.hwmgr.internal.hwsetup.EditText.getInstance(obj.ContentGrid);
            obj.EditBlockLabel.TextAlignment = 'left';
            obj.EditBlockLabel.Row = 3;
            obj.EditBlockLabel.Column = 2;
            obj.EditBlockLabel.ValueChangedFcn = @obj.captureBlockLabel;

            obj.LabelBlockLabel = matlab.hwmgr.internal.hwsetup.Label.getInstance(obj.ContentGrid);
            obj.LabelBlockLabel.Text = 'Block label:';
            obj.LabelBlockLabel.Row = 3;
            obj.LabelBlockLabel.Column = 1;

            obj.EditBlockDescription = matlab.hwmgr.internal.hwsetup.EditText.getInstance(obj.ContentGrid);
            obj.EditBlockDescription.TextAlignment = 'left';
            obj.EditBlockDescription.Row = 3;
            obj.EditBlockDescription.Column = [2,5];
            obj.EditBlockDescription.ValueChangedFcn = @obj.captureBlockDescription;

            obj.LabelBlockDescription = matlab.hwmgr.internal.hwsetup.Label.getInstance(obj.ContentGrid);
            obj.LabelBlockDescription.Text = 'Block description:';
            obj.LabelBlockDescription.Row = 3;
            obj.LabelBlockDescription.Column = 1;

            obj.RemoveButton.Row = 6;
            obj.RemoveButton.Column = 2;

            obj.MoveUpButton.Row = 6;
            obj.MoveUpButton.Column = 3;
            obj.MoveDownButton.Row = 6;
            obj.MoveDownButton.Column = 4;

            obj.AddBtn.Row = 6;
            obj.AddBtn.Column = 1;

            obj.Table = matlab.hwmgr.internal.hwsetup.Table.getInstance(obj.ContentGrid);
            obj.Table.Tag = 'Example_Table';
            obj.Table.Row = 5;
            obj.Table.Column = [1, 5];
            obj.Table.ColumnName = {'Name', 'Data type', 'Dimension','Initial value','Type'};
            obj.Table.ColumnEditable = logical([1 1 1 1 1]);
            obj.TableVal = obj.Table.getPeer();
            obj.TableVal.SelectionType = 'row';
            obj.TableVal.SelectionChangedFcn = @obj.selectionChangedFcn;
            obj.TableVal.CellEditCallback = @obj.validateTableData;

            slDataTypes = {...
                'int8',...
                'uint8',...
                'int16',...
                'uint16',...
                'int32',...
                'uint32',...
                'int64',...
                'uint64',...
                'single',...
                'double',...
                'boolean',...
                };
            obj.dataTypeStr = @(x)categorical({x}, unique([slDataTypes {x}], 'stable'));
            slTypes = {...
                'Non tunable',...
                'Tunable',...
                };
            obj.TypeStr = @(x)categorical({x}, unique([slTypes {x}], 'stable'));
            obj.Table.Data = table({"Mask_Input_1"},{obj.dataTypeStr('int8')}, {"[1,1]"},{'0'},{obj.TypeStr('Tunable')});
            updateScreen(obj);
            obj.HelpText.AboutSelection = '';
            obj.HelpText.WhatToConsider = 'Please enter the Block mask respective details in this screen. To decide about required Mask parameters open the required .ino file from the downloaded files and search for parameters. For example for adxl345 accelerometer sensor accelrange is one parameter which will be present in its adafruit github files. Mask parameters can be seen by double clicking the block after it gets generated.';
        end

        function id = getPreviousScreenID(~)
            id = 'SelectSourceFilesScreen';
        end

        function  id = getNextScreenID(obj)
            id = 'OutputScreen';
        end

        function reinit(obj)
            updateScreen(obj);
        end

        function show(obj)
            %Overwrite show method to hide Back button on Mac and Linux
            %since it is the entry screen for postinstall setup
            show@matlab.hwmgr.internal.hwsetup.TemplateBase(obj)
            if isunix
                obj.BackButton.Visible = 'off';
            end
        end

        function addRow(obj, ~, ~)
            obj.TableVal.Data =  [obj.TableVal.Data; table({"Mask_Input"},{obj.dataTypeStr('int8')}, {"[1,1]"},{'0'},{obj.TypeStr('Tunable')})];
        end

        function removeRow(obj,~,~)
            manageTableRows(obj.Table, 'remove');
        end

        function moveRowUp(obj, ~, ~)
            manageTableRows(obj.Table, 'moveup');
        end

        function moveRowDown(obj, ~, ~)
            manageTableRows(obj.Table, 'movedown');
        end

        function selectionChangedFcn(obj, src, ~)

        end

        function validateTableData(obj, ~, event)
            indices = event.Indices;
            newData = event.NewData;
            try
                if indices(2) == 4
                    param = 'Dimension';
                    numVal = newData;

                elseif indices(2) == 3
                    % Data type
                    unSupportedBuiltInDT = {'double','single'};
                    try
                        DataType = char(newData);
                        DataType = evalin('base', DataType);
                    catch
                        % built-in type
                    end

                    if isa(DataType, 'Simulink.AliasType')
                        try
                            DataType = DataType.BaseType;
                            DataType = evalin('base', DataType);
                        catch
                            % built-in type
                        end
                    end

                    switch class(DataType)
                        case'Simulink.NumericType'
                            % To do: Validate for word length
                        case 'char'
                            % To do: Validate for word length
                            if any(strcmpi(DataType, unSupportedBuiltInDT)) || isempty(regexp(DataType, '^[A-Za-z]\w+$', 'ONCE'))
                                error(message('soc:msgs:InvalidDataType', DataType).getString);
                            end
                        case 'double'
                        otherwise
                            error(message('soc:msgs:InvalidDataType', DataType).getString);
                    end
                end
            catch ME
                obj.DataInterfaces.Data.(['Var' num2str(indices(2))]){indices(1)} = event.PreviousData;
                obj.Workflow.throwError(ME.message);
            end
        end
    end

    methods(Access = 'private')
        function updateScreen(obj)

        end

        function updateSetupFlag(obj, src, ~)

        end

        function captureBlockName(obj, src, ~)
            obj.Workflow.BlockName = obj.EditBlockName.Text;
        end

        function captureBlockLabel(obj, src, ~)
            obj.Workflow.BlockLabel = obj.EditBlockLabel.Text;
        end

        function captureBlockDescription(obj, src, ~)
            obj.Workflow.BlockDescription = obj.EditBlockDescription.Text;
        end
    end

end
