classdef SelectSourceFilesScreen < BreadCrumb
    %SelectSourceFilesScreen is one screen that is meant
    %to be included in a package of screens that make up a Device Driver Builder app. It presents user a way to select the required source files for system object.

    % Copyright 2023 The MathWorks, Inc.


    properties(Access = public)
        ResultLabel
        CheckBoxList
    end
    
    properties(Access = private, Constant = true)
        FontSize = 13
        BreadCrumbStep = 3
    end
 
    methods(Access = 'public')
        function obj = SelectSourceFilesScreen(workflow)
            validateattributes(workflow, {'matlab.hwmgr.internal.hwsetup.Workflow'}, {});
            % obj@matlab.hwmgr.internal.hwsetup.TemplateBase(workflow,matlab.hwmgr.internal.hwsetup.TemplateLayout.GRID);         
            obj@BreadCrumb(workflow,matlab.hwmgr.internal.hwsetup.TemplateLayout.GRID);
            obj.setCurrentStep(obj.BreadCrumbStep);
            obj.ContentGrid.RowHeight = {'1x','1x'};
            obj.ContentGrid.ColumnWidth = {'1x', '1x', '1x', '1x'};
            obj.ResultLabel = matlab.hwmgr.internal.hwsetup.Label.getInstance(obj.ContentGrid);
            obj.ResultLabel.Text = "Select the required source(.c/.cpp) files from below list:";
            obj.ResultLabel.FontWeight = 'bold';
            obj.ResultLabel.Row = 1;
            obj.ResultLabel.Column = [1,  4];
            if obj.Workflow.FolderPath
            FileRows= obj.Workflow.FolderPath.Variables;
            FilesList={}
            for i=1:length(FileRows)
                files=dir(FileRows{i});
                for j=1:length(files)
                    if(contains(files(j).name,'.cpp') | contains(files(j).name,'.c'))
                        FilesList =[FilesList,fullfile(FileRows{i},files(j).name)];
                    end                 
                %FilesList =[FilesList,fullfile(files(j).name)];
                end
            end
            end
            % obj.CheckBoxList = matlab.hwmgr.internal.hwsetup.CheckBoxList.getInstance(obj.ContentGrid);
            % obj.CheckBoxList.Title="Select all";
            % % obj.CheckBoxList.ValueIndex = [1, 1];
            % obj.CheckBoxList.Row = 2;
            % obj.CheckBoxList.Column = [1,4];

            obj.CheckBoxList = matlab.hwmgr.internal.hwsetup.CheckBoxList.getInstance(obj.ContentPanel);
            obj.CheckBoxList.Title="Select all";
            obj.CheckBoxList.ValueIndex = [1, 1];
            obj.CheckBoxList.ColumnWidth = [25, 400];
            obj.CheckBoxList.Position=[20 50 445 280];
            if obj.Workflow.FolderPath
            obj.CheckBoxList.Items=FilesList;
            end
            obj.CheckBoxList.ValueChangedFcn = @obj.captureTheSelectedFiles;
            updateScreen(obj);
            obj.HelpText.AboutSelection = '';
            obj.HelpText.WhatToConsider = 'Please select the list of required src(.c/.cpp) files which are required for the block';
        end

        function id = getPreviousScreenID(~)
            id = 'SelectWorkingDirectoryScreen';
        end

        function  id = getNextScreenID(obj)
            id = 'BlockParametersScreen';
        end

        function reinit(obj)
            updateScreen(obj);
        end
        
        function show(obj)
            %Overwrite show method to hide Back button on Mac and Linux
            %since it is the entry screen for postinstall setup
            show@matlab.hwmgr.internal.hwsetup.TemplateBase(obj)
            if isunix
                obj.BackButton.Visible = 'off';
            end
        end
    end

    methods(Access = 'private')
        function updateScreen(obj)
          
        end
        
        function captureTheSelectedFiles(obj, ~, ~)
           obj.Workflow.SrcFiles = obj.CheckBoxList.Values;
        end
    end

end